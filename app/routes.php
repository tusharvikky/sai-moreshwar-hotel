<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.

CardNo: 5123456789012346
cvv:123
Expiry: May 2017


|
*/

Route::get('/', function()
{
//	return View::make('hello');
});

Route::controller('auth', 'AuthController');

Route::post('payment/booking', 'PaymentController@postPayBooking');
Route::post('payment/other', 'PaymentController@postOtherPayment');
Route::post('payment/success', 'PaymentController@postSuccess');
Route::get('payment/failed', 'PaymentController@getFailed');

Route::post('ajax/get-room-types', 'AjaxController@postRoomTypes');
Route::post('ajax/price-plan-manager', 'AjaxController@postGetPriceManager');
Route::post('ajax/get-room-count', 'AjaxController@postGetRoomCount');
Route::post('ajax/get-hotel-list', 'AjaxController@postHotelList');
Route::post('ajax/get-booking-price', 'AjaxController@postGetBookingPrice');

Route::group(array('before' => 'auth'), function()
{
	Route::controller('admin', 'AdminController');





});

