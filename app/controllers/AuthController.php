<?php

class AuthController extends BaseController {

	/**
	 * GET - /auth
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function getIndex()
	{
		if(Auth::check())
			return Redirect::to('admin');
		else
			return View::make('admin.login');		
	}

	/**
	 * GET - /auth/login
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function getLogin()
	{
		if(Auth::check())
			return Redirect::to('admin');
		else
			return View::make('admin.login');	
	}

	/**
	 * POST - /auth/login
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function postLogin()
	{
		// Declare the rules for the form validation.
		//
		$rules = array(
			'username'    => 'Required',
			'password' => 'Required'
		);

		// Get all the inputs.
		//
		$username = Input::get('username');
		$password = Input::get('password');

		// Validate the inputs.
		//
		$validator = Validator::make(Input::all(), $rules);

		// Check if the form validates with success.
		//
		if ($validator->passes())
		{
			// Try to log the user in.
			//
			if (Auth::attempt(array('username' => $username, 'password' => $password)))
			{
				// Redirect to the users page.
				//
				return Redirect::to('admin');
			}
			else
			{
				// Redirect to the login page.
				//
				return Redirect::to('auth/login');
			}
		}

		// Something went wrong.
		//dd($validator->messages());
		return Redirect::to('auth/login')->withErrors($validator->messages());
		
	}

	/**
	 * GET /auth/change-password
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function getChangePassword()
	{
		if(Auth::check())
		{
			return View::make('admin.changepassword');
		}else{
			return Redirect::to('auth/login');
		}
	}

	/**
	 * POST /auth/change-password
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function postChangePassword()
	{
		if(Auth::check())
		{
			$rules = array(
				'username'    => 'Required',
				'oldpassword' => 'Required',
				'password' => 'Required|confirmed',
			);

			// Get all the inputs.
			//
			$credentials = array(
						'username' => Input::get('username'),
						'password' => Input::get('oldpassword'));
			// Validate the inputs.
			//
			$validator = Validator::make(Input::all(), $rules);

			// Check if the form validates with success.
			//
			if ($validator->passes())
			{
				if(Auth::validate($credentials))
				{
					User::where('username', Input::get('username') )
							->update(
								array('password' => Hash::make( Input::get('password') )));

					return Redirect::to('admin');
				}
				return Redirect::to('auth/login');
			}

			return Redirect::to('auth/login');

		}else{
			return Redirect::to('auth/login');
		}
	}

	/**
	 * GET /auth/logout
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function getLogout()
	{
		Auth::logout();

		return Redirect::to('auth');
	}


}