<?php

class AjaxController extends BaseController {

    /**
     * POST /ajax/get-room-types
     *
     * @return void
     * @author tusharvikky
     **/
    public function postRoomTypes()
    {
    	if (Request::ajax())
    	{
	    	$hotelID = Input::get('hotelID');

	    	$rooms = Room::where('hotelID', $hotelID)->get(array('id','roomType'));
	    	$res = "<option> Choose Room Type </option>";
	    	if($rooms)
	    	{
	    		foreach($rooms as $room){
	    			$res .= "<option value='{$room->id}'>{$room->roomType}</option>";
                }
	    		return $res; 
	    	}
			
			return 'No Rooms Available';   		
    	}

    }

    /**
     * GET /ajax/price-plan-manager
     *
     * @return void
     * @author tusharvikky
     **/
    public function postGetPriceManager()
    {
        if (Request::ajax())
        {
            $hotelID = Input::get('hotelID');
            $roomType = Input::get('roomType');
            $data = array('extra', 'sat', 'sun', 'mon', 'tue', 'wed', 'thu', 'fri');
            $price = PriceManager::where('hotelID', $hotelID)->where('roomType', $roomType)->first($data);
            $res = '';
            
            if($price)
            {
                $price = $price->toArray();

                foreach($price as $key => $value)
                {
                    $res .= "<th> Rs. {$value} </th>";
                }
                $url = URL::to('admin/edit-price-manager/'.$hotelID.'/'.$roomType);
                $res .= "<th><a href={$url}><i class='icon-pencil'></i></a></th>";
                return $res;
            }
            else
            {
                return "<th colspan=8 style='text-align:center;''> Price N/A</th>";
            }

        }
    }

    /**
     * GET /ajax/get-room-count
     *
     * @return void
     * @author tusharvikky
     **/
    public function postGetRoomCount()
    {
        if(Request::ajax())
        {
            // get startBlock and endBlock
            $res = "<option> Select Rooms Count</option>";
            // 
            $hotelID = Input::get('hotelID');
            $roomType = Input::get('roomType');
            $date = explode(' - ',Input::get('date'));

            $totalRooms = RoomManager::where('hotelID', $hotelID)
                                    ->where('roomTypeID', $roomType)
                                    ->first(array('totalRooms'));
            if($totalRooms)
                $totalRooms = $totalRooms->toArray();

            for($i = (int)$totalRooms['totalRooms']; $i > 0; $i--)
            {
                $res .= "<option value='{$i}'>{$i}</option>";
            }
            
            return $res;
            
            // $roomsBlocked = RoomBlocking::where('hotelID', $hotelID)
            //                             ->where('roomType', $roomType)
            //                             ->sum('roomsBlocked');
        }
    }

    /**
     * POST /ajax/get-booking-price
     *
     * @return void
     * @author tusharvikky
     **/
    public function postGetBookingPrice()
    {
        if(Request::ajax())
        {
            $hotelID = Input::get('hotelID');
            $roomType = Input::get('roomType');
            $checkIn = Component::convertDateYMD(Input::get('input-check-in-date'));
            $checkOut = Component::convertDateYMD(Input::get('input-check-out-date'));
            $rooms = Input::get('roomCount');
            $adults = Input::get('adultCount');
            $child = Input::get('childCount');
            $email = Input::get('email');
            $extra = 0;
            $price4Day = 0;
            $totalDays = Component::daysDifference($checkIn, $checkOut);
            // $roomCount = 0;

            // If Hotel Blocked, return "Sorry, All Rooms filled."
            $hot = Hotel::where('id', (int)$hotelID)
                        ->where('is_blocked', 1)
                        //->where('is_deleted', (bool)0)
                        ->where('blockStart', '<=', $checkIn)
                        ->where('blockEnd', '>=', $checkOut)
                        //->first(array('blockStart', 'blockEnd'));
                        ->first();

            if(Component::isBlocked4Date($checkIn, $checkOut, @$hot->blockStart, @$hot->blockEnd))
            {
                return "Sorry, All Rooms filled.";
            }

            // If Rooms Blocked, return "Sorry, All Romms Filled."
            $blockedRooms = Booking::where('hotelID', (int)$hotelID)
                                ->where('roomType', (int)$roomType)
                                // ->whereBetween('bookStart', array($checkIn, $checkOut))
                                // ->whereBetween('bookEnd', array($checkIn, $checkOut))
                                ->where('bookStart', '<=', $checkIn)
                                ->where('bookEnd', '>=',$checkOut)
                                ->sum('roomCount');
            
            $roomCapacity = RoomManager::where('hotelID', (int)$hotelID)
                                    ->where('roomTypeID', (int)$roomType)
                                    ->first();

            if($blockedRooms >= $roomCapacity->totalRooms)
            {
                return "Sorry, All Rooms Filled.";
            }
            
            // If adults more then capacity of Room Type, 

            // dd($roomCapacity->adult);
                // If no. of adults  > 1, then return "Please Select +1 Room"
                if($adults > ($rooms * $roomCapacity->adult))
                {
                    // migrate person as extra from adults.
                    $extra = $adults - ($rooms * $roomCapacity->adult);
                    $adults = $adults - $rooms;
                    // dd($extra);
                    // Check still adults count more than room Capacity
                    // return error;
                    if($adults > ($rooms * $roomCapacity->adult))
                    {
                        return "Please select one more room.";
                    }
                }
                // If no. of extra  = 1, then add extra price and return Value.

            $price = PriceManager::where('hotelID', (int)$hotelID)
                                ->where('roomType', (int)$roomType)
                                ->first();


            // If Children more then capacity of Room Type, 
            // Charge extra for no. of childen.
            if($child > ($rooms * $roomCapacity->child))
            {
                $extraChild = $child - $roomCapacity->child;
                $price4Day = $price4Day + ((int) $extraChild * $price->extra);
            }
                //If children age more than 7, charge as extra adult.
                $adultchild = 0;
                $actualChild = 0;
                if(Input::get('txtchild')){
                    $childAge = array_filter(Input::get('txtchild'));
                
                    //$childAge = Input::get('txtchild');
                    // dd(count($childAge));
                    if(count($childAge) > 0)
                    {
                        // dd($extra);
                        foreach($childAge as $age)
                        {
                         if($age > 7)
                         {
                             $adultchild++;
                             $adults++;
                         }
                         if($age < 7)
                         {
                            $actualChild++;
                         }
                        }

                        if($actualChild > ($rooms * $roomCapacity->child))
                        {
                            return "Please select one more room.";
                        }


                        // dd($adultchild);
                        $extra += $adultchild;

                        if($extra > $rooms)
                        {
                          // dd($extra);
                          return "Please select one more room.";
                        }
                    }
                }
                //dd($adultchild);
                $price4Day = $price4Day + ($adultchild * $price->extra);

            // Else return Normal charges.
            if($totalDays > 1)
            {
                $fetchDay = $checkIn;
                for ($i=1; $i <= $totalDays; $i++) {

                    $day = Component::getCurrentWeekday($fetchDay);
                    $price4Day += $rooms * $price->$day;

                    $tmp = strtotime("+{$i} day", strtotime($fetchDay));
                    $fetchDay = date("Y/m/d", $tmp);
                }

            }
            else
            {
                $day = Component::getCurrentWeekday($checkIn);

                $price4Day = $rooms * $price->$day;
            }
            
            if($extra != 0)
            {
                $price4Day = $price4Day + ((int) $extra * (int) $price->extra);
            }
            
            // If Seasonal Date applicable, return amount of Special Prices.
            $offer = SpecialOffer::where('hotelID', (int)$hotelID)
                                ->where('roomType', (int)$roomType)
                                ->where('startDate', '<=', $checkIn)
                                ->where('endDate', '>=',$checkOut)
                                ->first();
            if($offer)
            {
                $price4Day = ((int) $rooms * $offer->rate) + ((int) $extra * $offer->extra);
            }
            
            $tax = Hotel::where('id', (int)$hotelID)
                        ->first(array('tax'));

            $tax = (float) $tax->tax / 100;

            $totalPrice = $price4Day + ($price4Day * $tax);

            return floor($totalPrice);


        }
    }

    /**
     * POST /ajax/get-hotel-list
     *
     * @return void
     * @author tusharvikky
     **/
    public function postHotelList()
    {
        if(Request::ajax())
        {
            $res = "<option> Select Hotel</option>";

            $hotels = Hotel::all();

            foreach($hotels as $hotel)
            {
                $res .= "<option value={$hotel->id}>{$hotel->name}</option>";
            }

            return $res;

        }
    }

    private function checkInDateRange($startDate, $endDate)
    {
        // Convert to mm/dd/yyyy
        $dates['startDate'] = $startDate;
        $dates['endDate'] = $endDate;
        // dd($dates);
        foreach($dates as $key => $value)
        {
            list($d, $m, $y) = preg_split('/\//', $value);

            $date[$key] = sprintf('%02d/%02d/%4d', $m, $d, $y);         
        }

        $user_ts = Date('m/d/Y');

        // Check that user date is between start & end
        return (($user_ts >= $date['startDate']) && ($user_ts <= $date['endDate']));
    }

}