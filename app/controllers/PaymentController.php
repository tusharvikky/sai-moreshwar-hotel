<?php

class PaymentController extends BaseController {

    /**
     * POST /payment/booking
     *
     * @return void
     * @author tusharvikky
     **/
    public function postPayBooking()
    {
        // $validation_rules = array(
        //     'hotelID' => 'Required',
        //     'roomType' => 'Required',
        //     );

        // $hotelID = Input::get('hotelID');
        // $roomType = Input::get('roomType');
        // $checkIn = Component::convertDateYMD(Input::get('input-check-in-date'));
        // $checkOut = Component::convertDateYMD(Input::get('input-check-out-date'));
        // $rooms = Input::get('roomCount');
        // $adults = Input::get('adultCount');
        // $child = Input::get('childCount');
        // $email = Input::get('email');
        
        $validation_rules = array(
            'name' => "Required",
            'email' => 'Required',
            'cnumber' => 'Required',
            'addr' => 'Required',
            'zipcode' => 'Required',
            'city' => 'Required',
            'state' => 'Required',
            'country' => 'Required',
            'amount' => 'Required|numeric',
            'hotelID' => 'Required',
            'roomType' => 'Required',
            'input-check-in-date' => 'Required',
            'input-check-out-date' => 'Required',
            'roomCount' => 'Required',
            'adultCount' => 'Required',
            'childCount' => 'Required'
            );

        $validator = Validator::make(Input::all(), $validation_rules);
        // dd(Input::all());
        if($validator->passes())
        {
            $inputs = Input::all();
            array_forget($inputs, '_token');
            $childCount = $inputs['txtchild'];
            array_forget($inputs, 'txtchild');

            $hotelName = Hotel::getHotel($inputs['hotelID']);
            $roomName = Room::getRoomType($inputs['roomType']);
            
            //$inputs = array_filter(array_map("trim", $inputs));
            $inputs['checkIn'] = Component::convertDateYMD($inputs['input-check-in-date']);
            $inputs['checkOut'] = Component::convertDateYMD($inputs['input-check-out-date']);
            array_forget($inputs, 'input-check-in-date');
            array_forget($inputs, 'input-check-out-date');
            // dd($inputs);
            $data['product'] = strtolower($hotelName->name.' '.$roomName->roomType.' '.$inputs['amount']);
            // Merchant key here as provided by Payu
            $data['key'] = "C0Dr8m";
            // Merchant Salt as provided by Payu
            $data['SALT'] = "3sf0jURk";
            // End point - change to https://secure.payu.in for LIVE mode
            $data['action'] = "https://test.payu.in/_payment";

            foreach($inputs as $key => $value)
            {
                $data[$key] = htmlentities($value, ENT_QUOTES);
            }

            if( empty($inputs['txnid']) )
            {
                  // Generate random transaction id
                $data['txnid'] = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            }
            else{
                $data['txnid'] = Input::get('txnid');
            }

            $hash = $data["key"].'|'.$data["txnid"].'|'.$inputs["amount"].'|'.$data["product"].'|'.$inputs["name"].'|'.$inputs["email"].'|'.$inputs["hotelID"].'|booking-payment|'.$data["roomType"].'|'.$data["checkIn"].'|'.$data["checkOut"].'|'.$data["roomCount"].'|'.$data["adultCount"].'|'.$data['childCount'].'|||'.$data["SALT"];
            // dd($hash);


            $data["hash"] = strtolower(hash('sha512', $hash));
            $data['surl'] = 'http://saimoreshwar.com/index.php/payment/success';
            $data['furl'] = 'http://saimoreshwar.com/index.php/payment/failed';
            // dd($data);
            Booking::saveBookingPayment($data);

            return View::make('payment.booking', $data);

        }
        else
        {
            return 'Something went wrong with your Form. Please Go back and check the form again.';
        }


    }

    /**
     * POST /payment/other
     *
     * @return void
     * @author tusharvikky
     **/
    public function postOtherPayment()
    {
        $validation_rules = array(
            'name' => "Required",
            'email' => 'Required',
            'cnumber' => 'Required',
            'addr' => 'Required',
            'zipcode' => 'Required',
            'city' => 'Required',
            'state' => 'Required',
            'country' => 'Required',
            'amount' => 'Required|numeric',
            );

        $validator = Validator::make(Input::all(), $validation_rules);

        if($validator->passes())
        {
            $inputs = Input::all();
            array_forget($inputs, '_token');
            $inputs = array_filter(array_map('trim', $inputs));
            // dd($inputs);
            $data['product'] = strtolower($inputs['name'].' '.$inputs['city'].' '.$inputs['amount']);
            // Merchant key here as provided by Payu
            $data['key'] = "C0Dr8m";
            // Merchant Salt as provided by Payu
            $data['SALT'] = "3sf0jURk";
            // End point - change to https://secure.payu.in for LIVE mode
            $data['action'] = "https://test.payu.in/_payment";

            foreach($inputs as $key => $value)
            {
                $data[$key] = htmlentities($value, ENT_QUOTES);
            }

            if( empty($inputs['txnid']) )
            {
                  // Generate random transaction id
                $data['txnid'] = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            }
            else{
                $data['txnid'] = Input::get('txnid');
            }

            $hash = $data["key"].'|'.$data["txnid"].'|'.$inputs["amount"].'|'.$data["product"].'|'.$inputs["name"].'|'.$inputs["email"].'|'.$inputs["cnumber"].'|other-payment|||||||||'.$data["SALT"];
            // dd($hash);


            $data["hash"] = strtolower(hash('sha512', $hash));
            $data['surl'] = 'http://saimoreshwar.com/index.php/payment/success';
            $data['furl'] = 'http://saimoreshwar.com/index.php/payment/failed';
            // dd($data);

            return View::make('payment.other', $data);

        }
        else
        {
            return 'Something went wrong with your Form. Please Go back and check the form again.';
        }
    }

    /**
     * POST /payment/success
     *
     * @return void
     * @author tusharvikky
     **/
    public function postSuccess()
    {
        $input = Input::all();
        if($input['udf2'] == 'other-payment')
        {
            //dd($input);
            $invoiceID = Booking::saveOtherPayment($input);
            $data = $input;
            $data['invoiceID'] = $invoiceID;
            
            Mail::send('emails.other', $data, function($message) use ($data)
            {
                $message->from('postmaster@saimoreshwar.mailgun.org', 'Sai Moreshwar');
                $message->to($data['email'], $data['firstname'])->subject('Payment Details - Hotel Sai Moreshwar');
            });

            return Redirect::to('http://saimoreshwar.com/Hotels/success.php');
            // Payment from Other Payment.
        }
        elseif($input['udf2'] == 'booking-payment')
        {
            //dd($input);
            $invoiceID = Booking::saveSuccessBooking($input);
            $data = $input;
            
            $txn = Booking::where('bookingID', $input['txnid'])->first()->toArray();
            $data['invoiceID'] = 'SAI000'.$invoiceID;
            $data['roomCount'] = $txn['roomCount'];
            $data['adultCount'] = $txn['adultCount'];
            $data['childCount'] = $txn['childCount'];

            $hotelName = Hotel::getHotel($input['udf1']);
            $roomName = Room::getRoomType($input['udf3']);
            $data['hotelName'] = $hotelName->name;
            $data['roomName'] = $roomName->roomType;
            
            Mail::send('emails.invoice', $data, function($message) use ($data)
            {
                $message->from('postmaster@saimoreshwar.mailgun.org', 'Sai Moreshwar');
                $message->to($data['email'], $data['firstname'])->subject('Payment Details - Hotel Sai Moreshwar');
            });

            return Redirect::to('http://saimoreshwar.com/Hotels/success.php');
        }

        return "Payment Success";
        //return Redirect::to('http://google.com');
    }

    /**
     * POST /payment/failed
     *
     * @return void
     * @author tusharvikky
     **/
    public function getFailed()
    {
        return Redirect::to('http://saimoreshwar.com/Hotels/index.php');
    }


}

/*
array(47) { 
["mihpayid"]=> string(18) "403993715508261926" 

["mode"]=> string(2) "CC" 
["status"]=> string(7) "success" 
["unmappedstatus"]=> string(8) "captured" 
["key"]=> string(6) "C0Dr8m" 
["txnid"]=> string(20) "7efc59c1532ed34cfe2d" 
["amount"]=> string(7) "5000.00" 
["discount"]=> string(4) "0.00" 
["addedon"]=> string(19) "2013-07-21 22:47:30" 
["productinfo"]=> string(27) "tushar deo navi mumbai 5000" 
["firstname"]=> string(10) "Tushar Deo" 
["lastname"]=> string(0) "" 
["address1"]=> string(46) "A-503 Lakhani Galaxy Sector -15 CBD Belapur" 
["address2"]=> string(0) "" 
["city"]=> string(11) "Navi Mumbai" 
["state"]=> string(11) "Maharashtra" 
["country"]=> string(5) "India" 
["zipcode"]=> string(6) "400614" 
["email"]=> string(21) "tusharvikky@gmail.com" 
["phone"]=> string(0) "" 
["udf1"]=> string(10) "1234567890" 
["udf2"]=> string(13) "other-payment" 
["udf3"]=> string(0) "" 
["udf4"]=> string(0) "" 
["udf5"]=> string(0) "" 
["udf6"]=> string(0) "" 
["udf7"]=> string(0) "" 
["udf8"]=> string(0) "" 
["udf9"]=> string(0) "" 
["udf10"]=> string(0) "" 
["hash"]=> string(128) "6dcb9a295b9321e0e0e945e1d7093ddf9fc818730181c78ffc559a34b9a9e628b2ea00be9cee72796b4e6871a161ea34410e6a9c83fee4c08a1e7d8a7df3ef27" 
["field1"]=> string(10) "2000077266" 
["field2"]=> string(12) "320303561745" 
["field3"]=> string(8) "20130722" 
["field4"]=> string(2) "MC" 
["field5"]=> string(6) "561745" 
["field6"]=> string(2) "00" 
["field7"]=> string(1) "0" 
["field8"]=> string(3) "3DS" 
["field9"]=> string(180) " Successful Verification of Secure Hash: -- Approved -- Transaction Successful -- The cardholder\'s Issuer was unable to authenticate due to some system error at the Issuer.--E317" 
["PG_TYPE"]=> string(4) "AXIS" 
["bank_ref_num"]=> string(10) "2000077266" 
["bankcode"]=> string(2) "CC" 
["error"]=> string(4) "E000" 
["name_on_card"]=> string(4) "test" 
["cardnum"]=> string(16) "512345XXXXXX2346" 
["cardhash"]=> string(64) "0e7312951f8f41718eb7e2f04a04c6766b13ed4f11700eb486f899026dc41e83" }







array(47) { 
["mihpayid"]=> string(18) "403993715508265688" 
["mode"]=> string(2) "CC" 
["status"]=> string(7) "success" 
["unmappedstatus"]=> string(8) "captured" 
["key"]=> string(6) "C0Dr8m" 
["txnid"]=> string(20) "ee2734cfd30f9a2fd799" 
["amount"]=> string(7) "3799.00" 
["discount"]=> string(4) "0.00" 
["addedon"]=> string(19) "2013-07-22 18:29:39" 
["productinfo"]=> string(35) "shirdi non ac standard triple 3799" 
["firstname"]=> string(10) "Tushar Deo" 
["lastname"]=> string(0) "" 
["address1"]=> string(46) "A-503 Lakhani Galaxy Sector -15 CBD Belapur" 
["address2"]=> string(0) "" 
["city"]=> string(11) "Navi Mumbai" 
["state"]=> string(11) "Maharashtra" 
["country"]=> string(5) "India" 
["zipcode"]=> string(6) "400614" 
["email"]=> string(21) "tusharvikky@gmail.com" 
["phone"]=> string(10) "1234567890" 
["udf1"]=> string(1) "2" 
["udf2"]=> string(15) "booking-payment" 
["udf3"]=> string(1) "5" 
["udf4"]=> string(10) "2013/07/25" 
["udf5"]=> string(10) "2013/07/27" 
["udf6"]=> string(0) "" 
["udf7"]=> string(0) "" 
["udf8"]=> string(0) "" 
["udf9"]=> string(0) "" 
["udf10"]=> string(0) "" 
["hash"]=> string(128) "c89e899062486a930b8e298c0b63068da92554fa910fa7d5f50e8821da238e63ed5d2c7bcb86e51322a199ae505a0491526fa31647e41ba4b446a49275ad4efb" 
["field1"]=> string(10) "2000077712" 
["field2"]=> string(12) "320323637631" 
["field3"]=> string(8) "20130722" 
["field4"]=> string(2) "MC" 
["field5"]=> string(6) "637631" 
["field6"]=> string(2) "00" 
["field7"]=> string(1) "0" 
["field8"]=> string(3) "3DS" 
["field9"]=> string(180) " Successful Verification of Secure Hash: -- Approved -- Transaction Successful -- The cardholder\'s Issuer was unable to authenticate due to some system error at the Issuer.--E317" 
["PG_TYPE"]=> string(4) "AXIS" 
["bank_ref_num"]=> string(10) "2000077712" 
["bankcode"]=> string(2) "CC" 
["error"]=> string(4) "E000" 
["name_on_card"]=> string(10) "test three" 
["cardnum"]=> string(16) "512345XXXXXX2346" 
["cardhash"]=> string(64) "0e7312951f8f41718eb7e2f04a04c6766b13ed4f11700eb486f899026dc41e83" }
*/