<?php

class AdminController extends BaseController {

	/**
	 * GET /admin/
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function getIndex()
	{
		$data['bookings'] = Booking::where('bookStart', '<=', date("Y/m/d"))
								->where('bookEnd', '>=', date("Y/m/d"))
								->where('isDeleted', 0)
								->where('paymentSuccess', 1)
								->orderBy('created_at', 'desc')
								->paginate(5);

		return View::make('admin.dashboard', $data);
	}

	/**
	 * GET /admin/add-hotel
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function getAddHotel()
	{
		$data['hotels'] = Hotel::all();
		return View::make('hotel.addhotel', $data);
	}

	/**
	 * POST /admin/add-hotel
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function postAddHotel()
	{
		$validator_rules = array(
			'name' => 'Required',
			'address' => 'Required',
			'city' => 'Required',
			'state' => 'Required',
			'country' => 'Required',
			'zip' => 'Required',
			'phone' => 'Required',
			'fax' => 'Required',
			'email' => 'Required|email',
			'tax' => 'Required');

		$validator = Validator::make(Input::all(), $validator_rules);

		if($validator->passes())
		{
			$hotel = Hotel::addHotel(Input::all());

			if($hotel)
			{
				return Redirect::to('admin/add-hotel');
			}
		}

		return Redirect::to('admin/add-hotel');	

	}
	
	/**
	 * GET /admin/edit-hotel
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function getEditHotel($id)
	{
		$data['editHotel'] = Hotel::find((int) $id);
		return View::make('hotel.addhotel', $data);
	}

	/**
	 * POST /admin/edit-hotel
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function postEditHotel()
	{
		
		$inputHotel = Input::all();
		array_forget($inputHotel, 'hotelID');
		array_forget($inputHotel, '_token');

		$validator_rules = array(
			'name' => 'Required',
			'address' => 'Required',
			'city' => 'Required',
			'state' => 'Required',
			'country' => 'Required',
			'zip' => 'Required',
			'phone' => 'Required',
			'fax' => 'Required',
			'email' => 'Required|email',
			'tax' => 'Required');

		$validator = Validator::make(Input::all(), $validator_rules);

		if($validator->passes())
		{
			$inputHotel = array_map('trim', $inputHotel);

			$hotel = Hotel::find((int) Input::get('hotelID'));
			$hotel->update($inputHotel);
			// dd($hotel);
			return Redirect::to('admin/add-hotel');			
		}

		return Redirect::to('admin/add-hotel');	
	}

	/**
	 * GET /admin/add-room-type
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function getAddRoomType()
	{
		$data['hotels'] = Hotel::all();

		$data['roomTypes'] = Room::all();

		return View::make('hotel.addroom', $data);
	}

	/**
	 * POST /admin/add-room-type
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function postAddRoomType()
	{

		$validator_rules = array(
			'hotelID' => 'Required',
			'roomType' => 'Required',
			'descriptionRoom' => 'Required');

		$validator = Validator::make(Input::all(), $validator_rules);

		if($validator->passes())
		{
			$room = Room::addRoomType(Input::all());

			return Redirect::to('admin/add-room-type');

		}

		return Redirect::to('admin/add-room-type');
	}

	/**
	 * GET /admin/edit-room-type
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function getEditRoomType($id)
	{
		$data['hotels'] = Hotel::all();

		$data['editroomType'] = Room::find((int) $id);

		return View::make('hotel.addroom', $data);
	}

	/**
	 * POST /admin/edit-room-type
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function postEditRoomType()
	{
		$roomType = Input::all();

		array_forget($roomType, 'RoomTypeID');
		array_forget($roomType, '_token');

		$validator_rules = array(
			'hotelID' => 'Required',
			'roomType' => 'Required',
			'descriptionRoom' => 'Required');

		$validator = Validator::make(Input::all(), $validator_rules);
		// dd($validator->passes());
		if($validator->passes())
		{
			$room = Room::find((int) Input::get('RoomTypeID'));
			$room->update($roomType);
			// dd((bool)$room);
			return Redirect::to('admin/add-room-type');
		}

		return Redirect::to('admin/add-room-type');
	}

	/**
	 * GET /admin/room-manager
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function getRoomManager()
	{
		$data['hotels'] = Hotel::all();

		$data['roomTypes'] = Room::all();

		$data['rooms'] = RoomManager::all();

		return View::make('hotel.roommanager', $data);
	}

	/**
	 * POST /admin/room-manager
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function postRoomManager()
	{
		$validator_rules = array(
			'hotelID' => 'Required',
			'roomTypeID' => 'Required',
			'adult' => 'Required',
			'child' => 'Required',
			'totalRooms' => 'Required');

		$validator = Validator::make(Input::all(), $validator_rules);
		// dd($validator->passes());
		if($validator->passes())
		{
			$rooms = RoomManager::addRooms(Input::all());

			return Redirect::to('admin/room-manager');
		}
		
		return Redirect::to('admin/room-manager');
	}

	/**
	 * GET /admin/edit-room-manager
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function getEditRoomManager($id)
	{
		$data['hotels'] = Hotel::all();

		$data['roomTypes'] = Room::all();

		// $data['rooms'] = RoomManager::all();

		$data['editRoomManager'] = RoomManager::find((int) $id);

		return View::make('hotel.roommanager', $data);
	}

	/**
	 * POST /admin/edit-room-manager
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	public function postEditRoomManager()
	{
		$roomManager = Input::all();
		array_forget($roomManager, '_token');
		array_forget($roomManager, 'roomManagerID');

		$validator_rules = array(
			'hotelID' => 'Required',
			'roomTypeID' => 'Required',
			'adult' => 'Required',
			'child' => 'Required',
			'totalRooms' => 'Required');

		$validator = Validator::make(Input::all(), $validator_rules);
		// dd($validator->passes());
		if($validator->passes())
		{
			$rooms = RoomManager::find((int) Input::get('roomManagerID'));
			$rooms->update($roomManager);

			return Redirect::to('admin/room-manager');
		}

		return Redirect::to('admin/room-manager');
	}

	/**
	 * GET admin/room-capacity
	 *
	 * @return void
	 * @author tusharvikky <tusharvikky@gmail.com>
	 **/
	// public function getRoomCapacity()
	// {
	// 	return View::make('hotel.roomcapacity');
	// }

	/**
	 * GET /admin/price-manager
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function getPriceManager()
	{
		$data['hotels'] = Hotel::all();

		$data['prices'] = PriceManager::all();

		// $data['roomTypes'] = Room::all();

		return View::make('price.pricemanager', $data);
	}

	/**
	 * POST /admin/price-manager
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function postPriceManager()
	{
		$price = PriceManager::where('hotelID', Input::get('hotelID'))->where('roomType', Input::get('roomType'))->first();
		// If Price Already in DB - DO NOT OVERWRITE IT - Show Error.
		if($price)
			return Redirect::to('admin/price-manager');

		$validator_rules = array(
			'hotelID' => 'Required',
			'roomType' => 'Required',
			'adult' => 'Required',
			'extra' => 'Required');

		$validator = Validator::make(Input::all(), $validator_rules);
		// dd($validator->passes());
		if($validator->passes())
		{
			$input['hotelID'] = Input::get('hotelID');
			$input['roomType'] = Input::get('roomType');
			$input['extra'] = Input::get('extra');

			foreach(Input::get('adult') as $key => $value)
			{
				$input[$key] = $value;
			}

			PriceManager::addPrice($input);

			return Redirect::to('admin/price-manager');
			// dd($input);

		}

	}
 
	/**
	 * GET /admin/edit-price-manager
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function getEditPriceManager($hotelID, $roomType)
	{
		$data['hotels'] = Hotel::all();
        // $data = array('extra', 'sat', 'sun', 'mon', 'tue', 'wed', 'thu', 'fri');
        $price = PriceManager::where('hotelID', $hotelID)->where('roomType', $roomType)->first();
        // dd($price);
        if($price)
        {
        	$data['editPriceManager'] = $price;
        }

		return View::make('price.pricemanager', $data);
	}

	/**
	 * POST /admin/edit-price-manager
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function postEditPriceManager()
	{
		$validator_rules = array(
			'hotelID' => 'Required',
			'roomType' => 'Required',
			'adult' => 'Required',
			'extra' => 'Required');

		$validator = Validator::make(Input::all(), $validator_rules);
		//dd(Input::all());
		if($validator->passes())
		{
			$input['hotelID'] = Input::get('hotelID');
			$input['roomType'] = Input::get('roomType');
			$input['extra'] = Input::get('extra');

			foreach(Input::get('adult') as $key => $value)
			{
				$input[$key] = $value;
			}

			PriceManager::editPrice($input);

			return Redirect::to('admin/price-manager');
		}
		return Redirect::to('admin/price-manager');
	}

	/**
	 * GET /admin/special-offer
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function getSpecialOffer()
	{
		$data['hotels'] = Hotel::all();

		$data['offers'] = SpecialOffer::all();
		return View::make('price.specialoffer', $data);
	}

	/**
	 * POST /admin/special-offer
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function postSpecialOffer()
	{
		// $data['hotels'] = Hotel::all();
		$data = Input::all();
		$validator_rules = array(
			'hotelID' => 'Required',
			'roomType' => 'Required',
			'offer' => 'Required',
			'date' => 'Required',
			'rate' => 'Required',
			'extra' => 'Required' );

		$validator = Validator::make(Input::all(), $validator_rules);
		if($validator->passes())
		{
			$date = explode(" - ", $data['date']);
			array_forget($data, '_token');
			array_forget($data, 'date');

			// Date in DD/MM/YYYY
			$data['startDate'] = Component::convertDateYMD($date[0]);
			$data['endDate'] = Component::convertDateYMD($date[1]);
			// dd($data);
			$data = array_map('trim', $data);

			SpecialOffer::addOffer($data);

			return Redirect::to('admin/special-offer');

		}
		return Redirect::to('admin/special-offer');

	}

	/**
	 * GET /admin/edit-special-offer
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function getEditSpecialOffer($id)
	{
		// $data['hotels'] = Hotel::all();

		// $data['offers'] = SpecialOffer::all();

		$data['editSpecialOffer'] = SpecialOffer::find((int) $id);
		return View::make('price.specialoffer', $data);
	}

	/**
	 * POST /admin/edit-special-offer
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function postEditSpecialOffer()
	{
		$data = Input::all();
		$validator_rules = array(
			'hotelID' => 'Required',
			'roomType' => 'Required',
			'offer' => 'Required',
			'date' => 'Required',
			'rate' => 'Required',
			'extra' => 'Required' );

		$validator = Validator::make(Input::all(), $validator_rules);
		if($validator->passes())
		{
			$date = explode(" - ", $data['date']);
			array_forget($data, '_token');
			array_forget($data, 'date');

			$data['startDate'] = $date[0];
			$data['endDate'] = $date[1];

			$data = array_map('trim', $data);
			
			SpecialOffer::where('hotelID', Inpur::get('hotelID'))
						->where('roomType', Inpur::get('roomType'))
						->update($data);
			
			return Redirect::to('admin/special-offer');

		}	
			
		return Redirect::to('admin/special-offer');
	}

	/**
	 * GET /admin/booking-list
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function getBookingList()
	{
		$data['bookings'] = Booking::where('isDeleted', 0)
				->where('isBlocked', 0)
				->where('paymentSuccess', 1)
				->where('roomCount', '!=', 0)
				->orderBy('created_at', 'desc')->paginate(5);

		return View::make('booking.bookinglist', $data);
	}

	/**
	 * GET /admin/other-payment
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function getOtherPayment()
	{
		$data['bookings'] = Booking::where('isDeleted', 0)
				->where('isBlocked', 0)
				->where('paymentSuccess', 1)
				->where('roomCount', '=', 0)
				->orderBy('created_at', 'desc')->paginate(5);

		return View::make('booking.otherpayment', $data);
	}

	/**
	 * GET /admin/delete-booking
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function getDeleteBooking($id)
	{
		Booking::find($id)
				->update(array('isDeleted' => 1));

		return Redirect::to('admin/booking-list');
	}	

	/**
	 * GET /admin/room-blocking
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function getRoomBlocking()
	{
		$data['hotels'] = Hotel::all();

		$data['roomsBlocked'] = Booking::where('isBlocked', 0)
									->where('isDeleted', 0)
									->get();

		return View::make('booking.roomblocking', $data);
	}

	/**
	 * POST /admin/room-blocking
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function postRoomBlocking()
	{
		$validator_rules = array(
			'name' => 'Required',
			'hotelID' => 'Required',
			'roomType' => 'Required',
			'roomsBlocked' => 'Required',
			'date' => 'Required');

		$validator = Validator::make(Input::all(), $validator_rules);

		if($validator->passes())
		{
			$date = explode(" - ", Input::get('date'));
			$input = array(
				'blockName' => Input::get('name'),
				'hotelID' => Input::get('hotelID'),
				'roomType' => Input::get('roomType'),
				'roomCount' => Input::get('roomsBlocked'),
				'bookStart' => $date[0],
				'bookEnd' => $date[1]);

			// RoomBlocking::createRoomBlock($input);
			Booking::createRoomBlock($input);

			return Redirect::to('admin/room-blocking');
		}
	}

	/**
	 * GET /admin/delete-room-blocking
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function getDeleteRoomBlocking($id)
	{

		Booking::find($id)
				->update(array('isDeleted'=> 1));

		return Redirect::to('admin/room-blocking');
	}

	/**
	 * GET /admin/hotel-blocking
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function getHotelBlocking()
	{
		$data['hotels'] = Hotel::all();
		// $data['hotels'] = Hotel::where('is_blocked', 0)->get();
		// $blockedHotels = array();
		// foreach (Hotel::all() as &$hotel) {
		// 	$blockedHotels[$hotel->id] = $this->checkInDateRange($hotel->blockStart, $hotel->blockEnd);
		// }

		// $data['hotels'] = Hotel::where('id', $blockedHotels)->get();

		$blocked = Hotel::where('is_blocked', 1)->get(array('id', 'name', 'blockStart', 'blockEnd'));
		if($blocked)
			$blocked = $blocked->toArray();

		foreach($blocked as &$block)
		{
			$block['inRange'] = Component::checkInDateRange($block['blockStart'], $block['blockEnd']);
			// dd($block['inRange']);
		}

		$data['blocked'] = $blocked;

		return View::make('booking.hotelblocking', $data);
	}
	
	/**
	 * POST /admin/hotel-blocking
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function postHotelBlocking()
	{
		$validator_rules = array(
			'hotelID' => 'Required',
			'date' => 'Required');

		$validator = Validator::make(Input::all(), $validator_rules);

		if($validator->passes())
		{
			$date = explode(" - ", Input::get('date'));
			$input = array(
				'is_blocked' => (bool)1,
				// 'blockStart' => Component::convertDateYMD($date[0]),
				// 'blockEnd' => Component::convertDateYMD($date[1])
				'blockStart' => $date[0],
				'blockEnd' => $date[1]
				);

			Hotel::where('id', Input::get('hotelID'))
				->update($input);

			return Redirect::to('admin/hotel-blocking');
		}

		return Redirect::to('admin/hotel-blocking');
	}

	/**
	 * GET /admin/delete-hotel-blocking/{1}
	 * http://www.google.com/ig/calculator?hl=en&q=1USD=?INR
	 * @return void
	 * @author tusharvikky
	 **/
	public function getDeleteHotelBlocking($hotelID)
	{
		Hotel::where('id', $hotelID)
				->update(array( 'is_blocked' => false ));

		return Redirect::to('admin/hotel-blocking');
	}

	/**
	 * POST /admin/tax-manager
	 * http://www.google.com/ig/calculator?hl=en&q=1USD=?INR
	 * @return void
	 * @author tusharvikky
	 **/
	// public function postTaxManager()
	// {
	// 	$validator_rules = array('tax' => 'Required');

	// 	$validator = Validator::make(Input::all(), $validator_rules);
	// }

	/**
	 * DEMO GET /admin/booking-price
	 *
	 * @return void
	 * @author tusharvikky
	 **/
	public function getBookingPrice()
	{
            $hotelID = Input::get('hotelID');
            $roomType = Input::get('roomType');
            $checkIn = Input::get('input-check-in-date');
            $checkOut = Input::get('input-check-out-date');
            $rooms = Input::get('roomCount');
            $adults = Input::get('adultCount');
            $child = Input::get('childCount');
            $email = Input::get('email');
            $extra = '1';
            $data['price'] = 0;

            // If Hotel Blocked, return "Sorry, All Rooms filled."
            $hot = Hotel::where('id', (int)$hotelID)
            			->where('is_blocked', 1)
            			->first(array('blockStart', 'blockEnd'));

            if(Component::isBlocked4Date($checkIn, $checkOut, @$hot->blockStart, @$hot->blockEnd))
            {
            	return "Sorry, All Rooms filled.";
            }

            // If Rooms Blocked, return "Sorry, All Romms Filled."
            $blockedRooms = Booking::where('hotelID', (int)$hotelID)
            					->where('roomType', (int)$roomType)
            					// ->whereBetween('bookStart', array($checkIn, $checkOut))
            					// ->whereBetween('bookEnd', array($checkIn, $checkOut))
            					->where('bookStart', '<=', $checkIn)
            					->where('bookEnd', '>=',$checkOut)
            					->sum('roomCount');
            
            $roomCapacity = RoomManager::where('hotelID', (int)$hotelID)
            						->where('roomTypeID', (int)$roomType)
            						->first();

            if($blockedRooms >= $roomCapacity->totalRooms)
            {
            	return "Sorry, All Rooms Filled.";
            }
            
            // If adults more then capacity of Room Type, 

            // dd($roomCapacity->adult);
                // If no. of adults  > 1, then return "Please Select +1 Room"
            	if($adults > $roomCapacity->adult)
            	{
            		return "Please select one more room.";
            	}
                // If no. of extra  = 1, then add extra price and return Value.

            $price = PriceManager::where('hotelID', (int)$hotelID)
            					->where('roomType', (int)$roomType)
            					->first();

            	if($extra != '0')
            	{
            		$data['price'] = (int) $extra * (int) $price->extra;
            	}

            // dd($data);
            // If Children more then capacity of Room Type, 
            // Charge extra for no. of childen.
            if($child > $roomCapacity->child)
            {
                $extraChild = $child - $roomCapacity->child;
                $data['price'] = $data['price'] + ((int) $extraChild * $price->extra);
            }
            	//If children age more than 7, charge as extra adult.
            	$adultchild = 0;
            	// foreach(Input::get('txtchild') as $age)
            	// {
            	// 	if($age > 7)
            	// 	{
            	// 		$adultchild++;
            	// 	}
            	// }

            	$data['price'] = $data['price'] + ($adultchild * $price->extra);

            // If Seasonal Date applicable, return amount of Special Prices.
            $offer = SpecialOffer::where('hotelID', (int)$hotelID)
            					->where('roomType', (int)$roomType)
            					->where('startDate', '<=', $checkIn)
            					->where('endDate', '>=',$checkOut)
            					->first();
            if($offer)
            {
            	$data['price'] = ((int) $adults * $offer->rate) + ((int) $extra * $offer->extra);
            }

            // Else return Normal charges.
            $day = Component::getCurrentWeekday();

			$data['price'] = $adults * $price->$day;

            return $data['price'];

	}

	private function checkInDateRange($startDate, $endDate)
	{
		// Convert to timestamp
		$dates['startDate'] = $startDate;
		$dates['endDate'] = $endDate;
		// dd($dates);
		foreach($dates as $key => $value)
		{
			list($d, $m, $y) = preg_split('/\//', $value);

			$date[$key] = sprintf('%02d/%02d/%4d', $m, $d, $y);			
		}

		$user_ts = Date('m/d/Y');

		// Check that user date is between start & end
		return (($user_ts >= $date['startDate']) && ($user_ts <= $date['endDate']));
	}
}