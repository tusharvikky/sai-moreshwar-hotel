<?php

class SpecialOffer extends Eloquent {
    protected $guarded = array();

    public static $rules = array();

    protected $table = 'special-offer';

    /**
     * Saves Special Offer in Database
     *
     * @return void
     * @author tusharvikky
     **/
    public static function addOffer($data)
    {
    	$offer = SpecialOffer::create($data);

    	return $offer->id;
    }
}