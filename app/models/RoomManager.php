<?php

class RoomManager extends Eloquent {
    protected $guarded = array();

    public static $rules = array();

    protected $table = 'room-manager';

    /**
     * Adds Rooms to Room Manager
     *
     * @return void
     * @author tusharvikky
     **/
    public static function addRooms($rooms)
    {
    	$rooms = RoomManager::create($rooms);

    	return $rooms->id;
    }
}