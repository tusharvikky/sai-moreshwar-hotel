<?php

class Booking extends Eloquent {
    protected $guarded = array();

    public static $rules = array();

    protected $table = 'bookings';

    /**
     * Creates a room Block.
     *
     * @return void
     * @author tusharvikky
     **/
    public static function createRoomBlock($data)
    {
    	$block = Booking::create($data);
    	return $block->id;
    }

    /**
     * Saves Payment details in Bookings Table
     *
     * @return void
     * @author tusharvikky
     **/
    public static function saveOtherPayment($data)
    {
    	//dd($data);
    	$dbData = array(
    		'bookingID' => $data['txnid'],
    		'paymentAmount' => $data['amount'],
    		'paymentType' => $data['mode'],
    		'firstName' => $data['firstname'],
    		'email' => $data['email'],
    		'address' => $data['address1'].'|'.$data['address2'].'|'.$data["city"].'|'.$data["state"].'|'.$data['country'].'|'.$data['zipcode'],
    		'phoneNumber' => $data['phone'],
    		'paymentSuccess' => 1,
    		'PaymentResponse' => implode(';', $data)
    		);

    	$book = Booking::create($dbData);
    	$id = $book->id;

    	$invoice = Booking::find($book->id)
    			->update(array('invoiceNumber' => 'SAI000'.$book->id));

    	return 'SAI000'.$book->id;
    }

    /**
     * Saves Booking Details in Booking Table
     *
     * @return void
     * @author tusharvikky
     **/
    public static function saveBookingPayment($data)
    {
        $dbData = array(
            'bookingID' => $data['txnid'],
            'paymentAmount' => $data['amount'],
            //'paymentType' => $data['mode'],
            'firstName' => $data['name'],
            'email' => $data['email'],
            //'address' => $data['address1'].' '.$data['address2'].' '.$data["city"].' '.$data["state"].' '.$data['country'].' '.$data['zipcode'],
            'address' => $data['addr'].'|'.$data['state'].'|'.$data['country'],
            'phoneNumber' => $data['cnumber'],
            'paymentSuccess' => 0,
            //'PaymentResponse' => implode(';', $data),

            //'bookingTime' => date('Y-m-d H:i:s'),
            'hotelID' => $data['hotelID'],
            'roomType' => $data['roomType'],
            'bookStart' => $data['checkIn'],
            'bookEnd' => $data['checkOut'],
            'adultCount' => $data['adultCount'],
            'childCount' => $data['childCount'],
            'totalCost' => $data['amount'],
            // 'comment' => $data[''],
            'roomCount' => $data['roomCount'],
            );

        $book = Booking::create($dbData);
        // $id = $book->id;

        // $invoice = Booking::find($book->id)
        //         ->update(array('invoiceNumber' => 'SAI000'.$book->id));

//        return 'SAI000'.$book->id;
        return true;
    }

    /**
     * Saves Data after success from payment.
     *
     * @return void
     * @author tusharvikky
     **/
    public static function saveSuccessBooking($data)
    {
        $book = Booking::where('bookingID', $data['txnid'])
                        ->update(array(
                            'paymentType' => $data['mode'],
                            'paymentSuccess' => 1,
                            'PaymentResponse' => implode(';', $data),
                            'bookingTime' => date('Y-m-d H:i:s'),
                            ));
        
        $bookid = Booking::where('bookingID', $data['txnid'])
                        ->first(array('id'))->toArray();
        
        $id = $bookid['id'];

        $invoice = Booking::find($id)
                ->update(array('invoiceNumber' => 'SAI000'.$id));

       return $id;

    }
}