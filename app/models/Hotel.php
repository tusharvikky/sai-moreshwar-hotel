<?php

class Hotel extends Eloquent {
    protected $guarded = array();

    public static $rules = array();

    /**
     * Adds Hotel to Database
     *
     * @return void
     * @author tusharvikky
     **/
    public static function addHotel($hotel)
    {
        $hotel = array_map('trim', $hotel);
    	$hotel = Hotel::create($hotel);

    	return $hotel->id;
    }

    /**
     * Returns Hotel Object
     *
     * @return void
     * @author tusharvikky
     **/
    public static function getHotel($id)
    {
        return Hotel::find((int) $id);
    }
}