<?php

class PriceManager extends Eloquent {
    protected $guarded = array();

    public static $rules = array();

    protected $table = 'price-manager';

    /**
     * Adds Price to Price Manager
     *
     * @return void
     * @author tusharvikky
     **/
    public static function addPrice($price)
    {
    	$p = PriceManager::create($price);
    	return $p->id;
    }

    /**
     * Edits the Prices of Room Types
     *
     * @return void
     * @author tusharvikky
     **/
    public static function editPrice($input)
    {
        $data = $input;
        array_forget($input, 'hotelID');
        array_forget($input, 'roomType');
        // dd($input);
        $save = PriceManager::where('hotelID', $data['hotelID'])->where('roomType', $data['roomType'])
                    ->update($input);
        if($save)
            return true;
    }
}