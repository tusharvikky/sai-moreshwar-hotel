<?php

class RoomBlocking extends Eloquent {
    protected $guarded = array();

    public static $rules = array();

    protected $table = 'roomBlocking';

    /**
     * Creates new Room Blocking
     *
     * @return void
     * @author tusharvikky
     **/
    public static function createRoomBlock($data)
    {
        $block = RoomBlocking::create($data);
        return $block->id;

    }


    /**
     * Counts No of Rooms Blocked for a Particular Range of Date
     *
     * @return void
     * @author tusharvikky
     **/
    public static function countRoomsBlockedForDate($blockStart, $blockEnd, $hotelID, $roomType)
    {
    	$totalRoomsBlocked = RoomBlocking::where('hotelID', $hotelID)
    									->where('roomType', $roomType)
    									->get(array('blockStart', 'blockEnd', 'roomsBlocked'));

    	// if($totalRoomsBlocked)
    	// 	$totalRoomsBlocked_arr = $totalRoomsBlocked->toArray();

    	// foreach($totalRoomsBlocked_arr)
    	// {
    	// 	RoomBlocking::checkInDateRange($totalRoomsBlocked_arr['blockStart'],$totalRoomsBlocked_arr['blockEnd']);
    	// }
    }

    public static function checkInDateRange($startDate, $endDate , $date)
    {
        // Convert to mm/dd/yyyy
        $dates['startDate'] = $startDate;
        $dates['endDate'] = $endDate;
        $dates['userDate'] = $date;
        // dd($dates);
        foreach($dates as $key => $value)
        {
            list($d, $m, $y) = preg_split('/\//', $value);

            $date[$key] = sprintf('%02d/%02d/%4d', $m, $d, $y);         
        }

        $user_ts = $date['userDate'];

        // Check that user date is between start & end
        return (($user_ts >= $date['startDate']) && ($user_ts <= $date['endDate']));
    }

    /**
     * Converts dd/mm/yyyy to mm/dd/yyyy
     *
     * @return void
     * @author tusharvikky
     **/
    public static function convertDate($date)
    {
        list($d, $m, $y) = preg_split('/\//', $date);

        return sprintf('%02d/%02d/%4d', $m, $d, $y);       
    }

    


}