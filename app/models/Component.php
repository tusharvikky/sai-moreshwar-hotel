<?php

class Component extends Eloquent {
    protected $guarded = array();

    public static $rules = array();

    /**
     * Converts dd/mm/yyyy to yyyy/mm/dd
     *
     * @return void
     * @author tusharvikky
     **/
    public static function convertDateYMD($date)
    {
        list($d, $m, $y) = preg_split('/\//', $date);

        return sprintf('%4d/%02d/%02d', $y, $m, $d);       
    }

    /**
     * Check if Date is in range with NOW.
     *
     * @return void
     * @author tusharvikky
     **/
    public static function checkInDateRange($startDate, $endDate)
    {
		// Convert to timestamp
		$dates['startDate'] = $startDate;
		$dates['endDate'] = $endDate;
		// dd($dates);
		// foreach($dates as $key => $value)
		// {
		// 	list($d, $m, $y) = preg_split('/\//', $value);

		// 	$date[$key] = sprintf('%4d/%02d/%02d', $y, $m, $d);			
		// }

		$user_ts = Date('Y/m/d');

		// Check that user date is between start & end
		return (($user_ts >= $dates['startDate']) && ($user_ts <= $dates['endDate']));
    }

    /**
     * Checks if checkIn and CheckOut date is in range with
     * two different Date Ranges.
     *
     * @return void
     * @author tusharvikky
     **/
    public static function isBlocked4Date($checkIn, $checkOut, $blockStart, $blockEnd)
    {
    	$datetime1 = new DateTime($checkIn);
		$datetime2 = new DateTime($checkOut);

		$datetime3 = new DateTime($blockStart);
		$datetime4 = new DateTime($blockEnd);
		// dd($blockStart);
		if ($blockStart == NULL && $blockEnd == NULL)
		{
			return false;
		}

		if ($datetime3 > $datetime1 && $datetime4 > $datetime1 && $datetime3 < $datetime2 && $datetime4 < $datetime2) {
			return false;
		}//end if

		return true;

		// Second Logic as Backup
		// $start = new DateTime($blockStart);
		// $end = new DateTime($blockEnd);

		// $required_start = new DateTime($checkIn);
		// $required_end = new DateTime($checkOut);
		// // dd($start < $required_start or $start > $required_end);
		// if ($end > $required_end or $end < $required_start)
		// {
		//   return true;
		// }

		// if ($start < $required_start or $start > $required_end)
		// {
		//   return true;
		// }

		// return false;
    }

    /**
     * gets current weekday like sat, sun, mon, etc.
     *
     * @return void
     * @author tusharvikky
     **/
    public static function getCurrentWeekday($date)
    {
    	$timestamp = strtotime($date);

    	return Str::lower(date('D', $timestamp));
    }

    /**
     * Calculates number of days between two dates.
     *
     * @return void
     * @author tusharvikky
     **/
    public static function daysDifference($date1, $date2)
    {
    	$date1 = new DateTime($date1);
		$date2 = new DateTime($date2);
		$interval = $date1->diff($date2);

		return $interval->days;
    }
}