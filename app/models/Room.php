<?php

class Room extends Eloquent {
    protected $guarded = array();

    public static $rules = array();

	protected $table = 'rooms-type';
    /**
     * Saves Room in DB.
     *
     * @return void
     * @author tusharvikky
     **/
    public static function addRoomType($room)
    {
    	$room = Room::create($room);

    	return $room->id;
    }

    /**
     * Returns RoomType Object
     *
     * @return void
     * @author tusharvikky
     **/
    public static function getRoomType($id)
    {
    	return Room::find((int) $id);
    }
}