<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIsBlockedToHotelsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotels', function(Blueprint $table) {
            $table->boolean('is_blocked');
            $table->string('blockStart');
            $table->string('blockEnd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotels', function(Blueprint $table) {
            $table->dropColumn('is_blocked');
            $table->dropColumn('blockStart');
            $table->dropColumn('blockEnd');
        });
    }

}
