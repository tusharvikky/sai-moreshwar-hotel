<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomBlockingTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roomBlocking', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
			$table->integer('hotelID');
			$table->integer('roomType');
			$table->string('blockStart');
			$table->string('blockEnd');
			$table->integer('roomsBlocked');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roomBlocking');
    }

}
