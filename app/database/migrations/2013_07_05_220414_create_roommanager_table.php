<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomManagerTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room-manager', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('hotelID');
			$table->integer('roomTypeID');
			$table->integer('adult');
			$table->integer('child');
			$table->integer('totalRooms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room-manager');
    }

}
