<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('bookingID');
			$table->datetime('bookingTime');
            $table->integer('hotelID');
            $table->integer('roomType');
			$table->string('bookStart');
			$table->string('bookEnd');
            $table->integer('adultCount');
			$table->integer('childCount');
			$table->integer('extraCount');
			$table->float('totalCost');
            $table->float('paymentAmount');
			$table->string('paymentType');
			$table->boolean('paymentSuccess');
            $table->string('invoiceNumber');

            $table->string('firstName');
            $table->string('lastName');
            $table->string('email');
            $table->string('address');
            $table->string('phoneNumber');
            $table->string('comment');
            
            $table->integer('roomCount');		
            $table->boolean('isBlocked');
			$table->boolean('isDeleted');
			$table->string('blockName');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }

}
