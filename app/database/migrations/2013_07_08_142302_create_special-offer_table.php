<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSpecialOfferTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special-offer', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('hotelID');
			$table->integer('roomType');
			$table->string('offer');
			$table->string('startDate');
			$table->string('endDate');
			$table->integer('price');
			$table->float('rate');
			$table->float('extra');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('special-offer');
    }

}
