<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHotelTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
			$table->text('address');
			$table->string('city');
            $table->string('state');
			$table->string('country');
			$table->string('zip');
			$table->string('phone');
			$table->string('fax');
			$table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hotels');
    }

}
