<?php

class AdminTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	// DB::table('admin')->delete();

        $admin = array(
        	'username' => 'admin',
        	'password' => Hash::make('greenworld'),
        	'first_name' => 'admin',
        	'last_name' => 'admin',
        	'email' => 'admin@admin.com',
        	'ip' => '127.0.0.1'
        );

        // Uncomment the below to run the seeder
        DB::table('admin')->insert($admin);
    }

}