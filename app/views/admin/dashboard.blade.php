@extends('layout.one')

@section('mainview')
	<h1>What's New Today</h1>
	<?php //dd(count($bookings)) ?>
	<hr class="bs-docs-separator">
	<span class="label label-success">
		We have {{count($bookings)}} Booking(s) Today.
	</span>
	<hr class="bs-docs-separator">
	<div class="row">

		<div class="span12">
			@foreach($bookings as $booking)
			<div class="row">
				<div class="span6">
					<span class="label label-important">BOOKING DETAILS</span></br>
					<b>Check In</b> : {{$booking->bookStart}}</br>
					<b>Check Out</b> : {{$booking->bookEnd}}</br>
					<b>Rooms</b> : {{$booking->roomCount}}</br>
					<b>Adults</b> : {{$booking->adultCount}}</br>
					<b>Childs</b> : {{$booking->childCount}}</br>					
				</div>
				<div class="span6">
					<span class="label label-important">CUSTOMER DETAILS</span></br>
					<b>Name</b>  : <span class="label label-info">{{$booking->firstName}}</span></br>
					<b>Address</b>  : {{$booking->address}}</br>
					<b>Email</b>  : {{$booking->email}}</br>
					<b>Contact</b>  : {{$booking->phoneNumber}}</br>					
				</div>			

			</div>
		</div>
		<hr class="bs-docs-separator">
		@endforeach


	</div>

	<div class="pagination pagination-centered pagination-large">
		{{$bookings->links();}}
	</div>
@stop