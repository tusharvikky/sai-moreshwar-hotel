@extends('layout.one')

@section('mainview')
	<h1>Change Password</h1>

	{{Form::open(array('url' => 'auth/change-password'))}}

<fieldset>

<!-- Form Name -->
<legend>Change Password</legend>

<!-- Text input-->
<div class="control-group">
  <label class="control-label">Username</label>
  <div class="controls">
    <input id="username" name="username" type="text" placeholder="Username" class="input-xlarge" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="control-group">
  <label class="control-label">Old Password</label>
  <div class="controls">
    <input id="password" name="oldpassword" type="password" placeholder="Old Password" class="input-xlarge" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="control-group">
  <label class="control-label">New Password</label>
  <div class="controls">
    <input id="password" name="password" type="password" placeholder="New Password" class="input-xlarge" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="control-group">
  <label class="control-label">Password Confimation</label>
  <div class="controls">
    <input id="password_confirmation" name="password_confirmation" type="password" placeholder="Password Confirmation" class="input-xlarge" required="">
    
  </div>
</div>

<!-- Button -->
<div class="control-group">
  <label class="control-label">Update</label>
  <div class="controls">
    <button id="save" name="save" class="btn btn-success">Update</button>
  </div>
</div>

</fieldset>


	{{Form::close()}}
@stop