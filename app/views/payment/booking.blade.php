<!DOCTYPE html>
<html>
<head>
  <title>SaiMoreshwar Payment</title>
  <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }

  </script>
</head>
<body onload="submitPayuForm();">
  <h1>Please Wait while we redirect you to Payment Gateway....</h1>
  <form action="{{$action}}" method="post" accept-charset="utf-8" name="payuForm">
  
  <input type="hidden" name="key" value="{{$key}}">
  <input type="hidden" name="hash" value="{{$hash}}">
  <input type="hidden" name="txnid" value="{{$txnid}}">
  <input type="hidden" name="amount" value="{{$amount}}">
  <input type="hidden" name="firstname" value="{{$name}}">
  <input type="hidden" name="email" value="{{$email}}">
  <input type="hidden" name="productinfo" value="{{$product}}">
  <input type="hidden" name="surl" value="{{$surl}}">
  <input type="hidden" name="furl" value="{{$furl}}">
  <input type="hidden" name="address1" value="{{$addr}}">
  <input type="hidden" name="city" value="{{$city}}">
  <input type="hidden" name="state" value="{{$state}}">
  <input type="hidden" name="country" value="{{$country}}">
  <input type="hidden" name="zipcode" value="{{$zipcode}}">
  <input type="hidden" name="phone" value="{{$cnumber}}">
  <input type="hidden" name="udf1" value="{{$hotelID}}">
  <input type="hidden" name="udf2" value="booking-payment">
  <input type="hidden" name="udf3" value="{{$roomType}}">
  <input type="hidden" name="udf4" value="{{$checkIn}}">
  <input type="hidden" name="udf5" value="{{$checkOut}}">
  <input type="hidden" name="udf6" value="{{$roomCount}}">
  <input type="hidden" name="udf7" value="{{$adultCount}}">
  <input type="hidden" name="udf8" value="{{$childCount}}">
<!--   <input type="hidden" name="udf9" value="{{$hotelID}}">
  <input type="hidden" name="udf10" value="{{$hotelID}}"> -->
  </form>
</body>
</html>