@extends('layout.one')

@section('mainview')
	<h1>TAX Manager</h1>

	{{Form::open(array('url' => '/admin/tax-manager'))}}
	<fieldset>

	<!-- Form Name -->
	<legend>Save Tax Rate</legend>

	<!-- Appended Input-->
	<div class="control-group">
	  <label class="control-label">TAX Rate</label>
	  <div class="controls">
	    <div class="input-append">
	      <input id="tax" name="tax" class="span2" placeholder="TAX" type="text" required="">
	      <span class="add-on">%</span>
	    </div>
	    
	  </div>
	</div>

	<!-- Button -->
	<div class="control-group">
	  <label class="control-label">Save</label>
	  <div class="controls">
	    <a href="#" class="btn btn-inverse">Save TAX</a>
	  </div>
	</div>

	</fieldset>
	</form>


@stop