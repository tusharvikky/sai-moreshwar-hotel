@extends('layout.one')

@section('mainview')
	<h1>Booking List</h1>

	@foreach($bookings as $booking)
	<div class="row">
							
		<?php $name = Hotel::getHotel($booking->hotelID); ?>
		<?php $room = Room::getRoomType($booking->roomType); ?>
		<div class="span4">
			<span class="label label-important">HOTEL DETAILS</span></br>
			<b>Hotel</b> : {{$name['name']}} </br>
			<b>Room Type</b> :{{$room['roomType']}}</br>
			<b>Reference No</b>  :{{$booking->invoiceNumber}}</br>
			<b>Order Date</b>  : {{date("F j, Y, g:i a", strtotime($booking->bookingTime))}}</br>
			<b>Order Amount</b>  : Rs.{{$booking->paymentAmount}}</br>
<!-- 			Comments :
			Standard Triple Bedroom with Mattress Rs 5150/- 4 people -->
			<span class="label label-important">TRANSACTION DETAILS</span> </br>
			<b>Invoice Id</b> :{{$booking->invoiceNumber}}</br>
			<b>Transaction Id</b> :{{$booking->bookingID}}</br>
			<b>Payment Type</b> : @if($booking->paymentType == 'CC') {{'Credit Card'}} @elseif($booking->paymentType == 'NB') {{'Net Banking'}} @elseif($booking->paymentType == 'DC'){{ 'Debit Card' }} @endif</br>
		</div>
		<div class="span4">
			<span class="label label-important">BOOKING DETAILS</span></br>
			<b>Check In</b> : {{$booking->bookStart}}</br>
			<b>Check Out</b> : {{$booking->bookEnd}}</br>
			<b>Rooms</b> : {{$booking->roomCount}}</br>
			<b>Adults</b> : {{$booking->adultCount}}</br>
			<b>Childs</b> : {{$booking->childCount}}</br>
		</div>
		<div class="span3">
			<span class="label label-important">CUSTOMER DETAILS</span></br>
			<b>Name</b>  : <span class="label label-info">{{$booking->firstName}}</span></br>
			<b>Address</b>  : {{$booking->address}}</br>
			<b>Email</b>  : {{$booking->email}}</br>
			<b>Contact</b>  : {{$booking->phoneNumber}}</br>
		</div>
		<div class="span1">
			<th><a href="{{URL::to('admin/delete-booking/'.$booking->id)}}"><i class='icon-remove'></i></a></th>
		</div>

	</div>
	<hr class="bs-docs-separator">
	@endforeach

	<div class="pagination pagination-centered pagination-large">
		{{$bookings->links();}}
	</div>




@stop