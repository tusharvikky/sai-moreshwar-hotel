@extends('layout.one')

@section('mainview')
	<h1>Room Blocking</h1>

	@if(!empty($roomsBlocked))
	<table class="table well">
		<thead>
			<tr>
				<th>Name</th>
				<th>Hotel Name</th>
				<th>Room Type</th>
				<th>Start Date</th>
				<th>End Date</th>
				<th>No of Rooms Blocked</th>
				<th style="width: 36px;"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($roomsBlocked as $block)
			<tr id="">
				<td>{{$block->blockName}}</td>
				<td><?php $name = Hotel::getHotel($block->hotelID); ?>{{$name["name"]}}</td>
				<td><?php $room = Room::getRoomType($block->roomType) ?>{{$room["roomType"]}}</td>
				<td>{{$block->bookStart}}</td>
				<td>{{$block->bookEnd}}</td>
				<td>{{$block->roomCount}}</td>
				<!-- <td>{{($block['inRange']) ? 'YES' : 'NO'}}</td> -->
				<td><a href="{{URL::to('admin/delete-room-blocking/'.$block['id'])}}"><i class="icon-pencil"></i></a></td>
			</tr>
			@endforeach
		</tbody>
	</table>		
	@endif
	




	{{Form::open(array('url' => '/admin/room-blocking'))}}
	<fieldset>

	<!-- Form Name -->
	<legend>Room Blocking</legend>

	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label">Name</label>
	  <div class="controls">
	    <input id="name" name="name" type="text" placeholder="Name" class="input-xlarge" required="">
	    
	  </div>
	</div>

	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label">Date</label>
	  <div class="controls">
	    <input id="date" name="date" type="text" placeholder="Date" class="input-xlarge" required="">
	    
	  </div>
	</div>

	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">Hotel</label>
	  <div class="controls">
	    <select id="hotelID" name="hotelID" class="input-xlarge" required="">
	      <option>Select Hotel</option>
	      	@foreach($hotels as $hotel)
			<option value="{{$hotel->id}}">{{$hotel->name}}</option>
			@endforeach
	    </select>
	  </div>
	</div>

	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">Room Type</label>
	  <div class="controls">
	    <select id="roomType" name="roomType" class="input-xlarge" required="">

	    </select>
	  </div>
	</div>

	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">Rooms to Block</label>
	  <div class="controls">
	    <select id="roomsBlocked" name="roomsBlocked" class="input-xlarge" required="">

	    </select>
	  </div>
	</div>

	<!-- Button -->
	<div class="control-group">
	  <label class="control-label">Block</label>
	  <div class="controls">
	    <button id="" name="" class="btn btn-danger">Block</button>
	  </div>
	</div>

	</fieldset>
	{{Form::close()}}
@stop

@section('js')
	@parent
	{{HTML::script('assets/moment.min.js')}}
	{{HTML::script('assets/daterangepicker.js')}}
	{{HTML::style('assets/daterangepicker.css')}}

	<script type="text/javascript">
	$('#hotelID').on('change', function(){
		var hotelSelect = $(this);
		var hotelID = $(this).val();
		$.ajax({
			url: "{{URL::to('/ajax/get-room-types')}}",
			type: "POST",
			data: { hotelID: hotelID}
		}).done(function(html){
			hotelSelect.closest('.control-group').next('.control-group').find('#roomType').html(html);
		});

	});

	$('#roomType').on('change', function(){
		var roomType = $(this);
		var roomTypeID = $(this).val();
		var hotelID = roomType.closest('.control-group').prev('.control-group').find('#hotelID');
		var valhotelID = hotelID.val();
		var date = hotelID.closest('.control-group').prev('.control-group').find('#date').val();
		$.ajax({
			url: "{{URL::to('/ajax/get-room-count')}}",
			type: "POST",
			data: { hotelID: valhotelID, roomType: roomTypeID, date: date}
		}).done(function(html){
			roomType.closest('.control-group').next('.control-group').find('#roomsBlocked').html(html);
		});
	})

	$(document).ready(function() {
	  $('input[name="date"]').daterangepicker();
	});
	</script>

@stop