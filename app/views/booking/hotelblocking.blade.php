@extends('layout.one')

@section('mainview')
	<h1>Hotel Blocking</h1>

	@if(!empty($blocked))
	<table class="table well">
		<thead>
			<tr>
				<!-- <th>For</th> -->
				<th>Hotel Name</th>
				<th>Start Date</th>
				<th>End Date</th>
				<th>Is Blocking Applicable NOW</th>
				<th style="width: 36px;"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($blocked as $block)
			<tr id="">
				<td>{{$block['name']}}</td>
				<td>{{$block['blockStart']}}</td>
				<td>{{$block['blockEnd']}}</td>
				<td>{{($block['inRange']) ? 'YES' : 'NO'}}</td>
				<td><a href="{{URL::to('admin/delete-hotel-blocking/'.$block['id'])}}"><i class="icon-pencil"></i></a></td>
			</tr>
			@endforeach
		</tbody>
	</table>		
	@endif


	{{Form::open(array('admin/hotel-blocking'))}}
	<fieldset>

	<!-- Form Name -->
	<legend>Hotel Blocking</legend>

	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">Hotel</label>
	  <div class="controls">
	    <select id="hotelID" name="hotelID" class="input-xlarge" required="">
	      <option>Select Hotel</option>
	      @foreach($hotels as $hotel)
	      <option value="{{$hotel->id}}">{{$hotel->name}}</option>
	      @endforeach
	    </select>
	  </div>
	</div>

	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label">Date</label>
	  <div class="controls">
	    <input id="date" name="date" type="text" placeholder="Date" class="input-xlarge" required="">
	    
	  </div>
	</div>

	<!-- Button -->
	<div class="control-group">
	  <label class="control-label">Block</label>
	  <div class="controls">
	    <button id="" name="" class="btn btn-danger">Block</button>
	  </div>
	</div>

	</fieldset>
	{{Form::close()}}

@stop

@section('js')
	@parent
	{{HTML::script('assets/moment.min.js')}}
	{{HTML::script('assets/daterangepicker.js')}}
	{{HTML::style('assets/daterangepicker.css')}}

	<script type="text/javascript">

	$(document).ready(function() {
	  $('input[name="date"]').daterangepicker();
	});
	</script>

@stop