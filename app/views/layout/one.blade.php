
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Sai Moreshwar</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="tusharvikky">
    <!--
        ▌│█║▌║▌║Author: tusharvikky║▌║▌║█│▌ 
        ▌│█║▌║▌║Web: http://tusharvikky.github.io/ ║▌║▌║█│▌ 
    -->
    <!-- Le styles -->
    {{HTML::style('bootstrap/css/bootstrap.min.css');}}
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
      /* Customize the navbar links to be fill the entire space of the .navbar */
      .menu .navbar .navbar-inner {
        padding: 0;
      }
      .menu .navbar .nav {
        margin: 0;
        display: table;
        width: 100%;
      }
/*      .menu .navbar .nav li {
        display: table-cell;
        width: 1%;
        float: none;
      }*/
      .menu .navbar .nav li a {
        font-weight: bold;
        text-align: center;
        border-left: 1px solid rgba(255,255,255,.75);
        border-right: 1px solid rgba(0,0,0,.1);
      }
      .menu .navbar .nav li:first-child a {
        border-left: 0;
        border-radius: 3px 0 0 3px;
      }
      .menu .navbar .nav li:last-child a {
        border-right: 0;
        border-radius: 0 3px 3px 0;
      }
    </style>

  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="{{URL::to('admin')}}">Sai Moreshwar Management</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
              Do you want to <a href="{{URL::to('auth/logout')}}" class="navbar-link" style="color: #FFFFFF;">Logout ?</a>
            </p>
            <ul class="nav">
              <li class="active"><a href="{{URL::to('admin')}}">Home</a></li>
<!--               <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li> -->
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">

      <div class="menu">

      <div class="navbar">
          <div class="navbar-inner">
            <div class="container">
              <ul class="nav">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Hotel Manager
                    <b class="caret"></b>
                  </a>
                    <ul class="dropdown-menu">
                      <li><a href="{{URL::to('admin/add-hotel')}}">Add Hotel</a></li>
                      <li><a href="{{URL::to('admin/add-room-type')}}">RoomType Manager</a></li>
                      <!-- <li><a href="{{URL::to('admin/room-capacity')}}">Capacity Manager</a></li> -->
                      <li><a href="{{URL::to('admin/room-manager')}}">Room Manager</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Price Manager
                    <b class="caret"></b>
                  </a>
                    <ul class="dropdown-menu">
                      <li><a href="{{URL::to('admin/price-manager')}}">Price Plan Manager</a></li>
                      <li><a href="{{URL::to('admin/special-offer')}}">Special Offer</a></li>
                      <!-- <li><a href="#">RoomType Manager</a></li> -->
                    </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Booking Manager
                    <b class="caret"></b>
                  </a>
                    <ul class="dropdown-menu">
                      <li><a href="{{URL::to('admin/booking-list')}}">Booking List</a></li>
                      <li><a href="{{URL::to('admin/other-payment')}}">Other Payment</a></li>
                      <li><a href="{{URL::to('admin/hotel-blocking')}}">Hotel Blocking</a></li>
                      <li><a href="{{URL::to('admin/room-blocking')}}">Room Blocking</a></li>
                      <!-- <li><a href="#">Capacity Manager</a></li> -->
                    </ul>
                </li>
<!--                 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    TAX
                    <b class="caret"></b>
                  </a> -->
                    <!-- <ul class="dropdown-menu"> -->
                      <!-- <li><a href="{{URL::to('admin/tax-manager')}}">TAX Manager</a></li> -->
                      <!-- <li><a href="#">Room Manager</a></li> -->
                      <!-- <li><a href="#">RoomType Manager</a></li> -->
                      <!-- <li><a href="#">Capacity Manager</a></li> -->
                    <!-- </ul> -->
                <!-- </li> -->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Setting
                    <b class="caret"></b>
                  </a>
                    <ul class="dropdown-menu">
                      <li><a href="{{URL::to('auth/change-password')}}">Change Password</a></li>
<!--                       <li><a href="#">Room Manager</a></li>
                      <li><a href="#">RoomType Manager</a></li>
                      <li><a href="#">Capacity Manager</a></li> -->
                    </ul>
                </li>
<!--                 <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li> -->
              </ul>
            </div>
          </div>
        </div>

        </div><!-- /menu -->
      @yield('mainview')

    </div> <!-- /container -->

    @section('modal')
    @show

    @section('js')
    <!-- Le javascript -->
    <script src="http://code.jquery.com/jquery.js"></script>
    {{HTML::script('bootstrap/js/bootstrap.min.js')}}
    @show
  </body>
</html>
