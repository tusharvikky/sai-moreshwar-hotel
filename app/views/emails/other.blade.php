<table
style="font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;"
cellpadding="4"
cellspacing="1">
    <tbody>
        <tr>
            <td
            align="left"
            style="font-weight:bold; font-variant:small-caps; background:#eeeeee;"
            colspan="4">Booking
                Details</td>
        </tr>
        <tr>
            <td
            align="left"
            style="background:#ffffff;">Booking
                Number</td>
                <td
                align="left"
                style="background:#ffffff;"
                colspan="3">{{$invoiceID}}</td>
        </tr>
        <tr>
            <td
            align="left"
            style="background:#ffffff;">Customer
                Name</td>
                <td
                align="left"
                style="background:#ffffff;"
                colspan="3">{{$firstname}}</td>
        </tr>
        <tr
        height="8px;">
            <td
            align="left"
            style="background:#ffffff;"
            colspan="4"></td>
                </tr>
                <tr>
                    <td
                    align="center"
                    style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">Check-In
                        Date</td>
                        <td
                        align="center"
                        style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">Check-Out
                            Date</td>
                            <td
                            align="center"
                            style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">Total
                                Nights</td>
                                <td
                                align="center"
                                style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">Total
                                    Rooms</td>
                </tr>
                <tr>
                    <td
                    align="center"
                    style="background:#ffffff;">-</td>
                        <td
                        align="center"
                        style="background:#ffffff;">-</td>
                            <td
                            align="center"
                            style="background:#ffffff;">-</td>
                                <td
                                align="center"
                                style="background:#ffffff;">-</td>
                </tr>
                <tr
                height="8px;">
                    <td
                    align="left"
                    style="background:#ffffff;"
                    colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td
                            align="center"
                            style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">Number</td>
                                <td
                                align="center"
                                style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">
                                    Type</td>
                                    <td
                                    align="center"
                                    style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">Max
                                        Occupancy</td>
                                        <td
                                        align="right"
                                        style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">Gross
                                            Total</td>
                        </tr>
                        <tr>
                            <td
                            align="center"
                            style="background:#ffffff;">1</td>
                                <td
                                align="center"
                                style="background:#ffffff;">Other Payment</td>
                                    <td
                                    align="center"
                                    style="background:#ffffff;">-</td>
                                        <td
                                        align="right"
                                        style="background:#ffffff;">Rs.{{$amount}}</td>
                        </tr>
                        <tr
                        height="8px;">
                            <td
                            align="left"
                            style="background:#ffffff;"
                            colspan="4"></td>
                                </tr>
                                <tr>
                                    <td
                                    colspan="3"
                                    align="right"
                                    style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">Sub
                                        Total</td>
                                        <td
                                        align="right"
                                        style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">Rs.{{$amount}}</td>
                                </tr>
                                <tr>
                                    <td
                                    colspan="3"
                                    align="right"
                                    style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">Grand
                                        Total</td>
                                        <td
                                        align="right"
                                        style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">Rs.{{$amount}}</td>
                                </tr>
    </tbody>
    </table>
    <br
    />
    <table
    style="font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;"
    cellpadding="4"
    cellspacing="1">
        <tr>
            <td
            align="left"
            colspan="2"
            style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">Payment
                Details</td>
        </tr>
        <tr>
            <td
            align="left"
            width="30%"
            style="font-weight:bold; font-variant:small-caps;background:#ffffff;">Payment
                Option</td>
                <td
                align="left"
                style="background:#ffffff;">@if($mode == 'CC') {{'Credit Card'}} @elseif($mode == 'NB') {{'Net Banking'}} @elseif($mode == 'DC'){{ 'Debit Card' }} @endif</td>
        </tr>
        <tr>
            <td
            align="left"
            width="30%"
            style="font-weight:bold; font-variant:small-caps;background:#ffffff;">Card
                Number</td>
                <td
                align="left"
                style="background:#ffffff;">{{$cardnum}}</td>
        </tr>
        </table>