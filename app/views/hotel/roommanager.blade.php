@extends('layout.one')

@section('mainview')
	<h1>Room Manager</h1>
	
	@if(!empty($rooms))
	<div class="btn-toolbar">
	    <a class="btn btn-primary" href="#addRoom">New Room</a>
<!-- 	    <button class="btn">Import</button>
	    <button class="btn">Export</button> -->
	</div>
	<div class="well">
	    <table class="table">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>Hotel Name</th>
	          <th>Room Type</th>
	          <th>Adult</th>
	          <th>Child</th>
	          <th>Total Rooms</th>
	          <th style="width: 36px;"></th>
	        </tr>
	      </thead>
	      <tbody>
	      	@foreach($rooms as $room)
	        <tr>
	          <td>{{$room->id}}</td>
	          <td><?php $name = Hotel::getHotel($room->hotelID) ?>{{$name["name"]}}</td>
	          <td><?php $roomtype = Room::getRoomType($room->roomTypeID); ?>{{$roomtype['roomType']}}</td>
	          <td>{{$room->adult}}</td>
	          <td>{{$room->child}}</td>
	          <td>{{$room->totalRooms}}</td>
	          <td>
	              <a href="{{URL::to('admin/edit-room-manager/'.$room->id)}}"><i class="icon-pencil"></i></a>
	              <a href="#myModal" role="button" data-toggle="modal" data-hotel="{{$room->id}}"><i class="icon-remove"></i></a>
	          </td>
	        </tr>
	        @endforeach
	      </tbody>
	    </table>
	</div>
	@endif
	
	@if(!empty($rooms))
	<div id="addRoom">
	{{Form::open(array('url' => 'admin/room-manager'))}}
	@else
	{{Form::open(array('url' => 'admin/edit-room-manager'))}}
	@endif

	<fieldset>

	<!-- Form Name -->
	<legend>Room Manager</legend>

	@if(!empty($editRoomManager))
	<input type="hidden" name="roomManagerID" value="{{ $editRoomManager->id }}">
	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">Hotels</label>
	  <div class="controls">
	    <select id="hotelID" name="hotelID" class="input-xlarge">
			<option value="{{$editRoomManager->hotelID}}"><?php $name = Hotel::getHotel($editRoomManager->hotelID); ?>{{$name["name"]}}</option>
	    </select>
	  </div>
	</div>
	@else
	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">Hotels</label>
	  <div class="controls">
	    <select id="hotelID" name="hotelID" class="input-xlarge">
	    	<option selected="selected">Select Hotel</option>
		@foreach($hotels as $hotel)
			<option value="{{$hotel->id}}">{{$hotel->name}}</option>
		@endforeach
	    </select>
	  </div>
	</div>
	@endif

	@if(!empty($editRoomManager))
	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">Room Type</label>
	  <div class="controls">
	    <select id="roomTypeID" name="roomTypeID" class="input-xlarge">
			<option value="{{$editRoomManager->roomTypeID}}"><?php $room = Room::getRoomType($editRoomManager->roomTypeID); ?>{{$room['roomType']}}</option>
	    </select>
	  </div>
	</div>
	@else
	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">Room Type</label>
	  <div class="controls">
	    <select id="roomTypeID" name="roomTypeID" class="input-xlarge">
<!-- 		@foreach($roomTypes as $roomType)
			<option value="{{$roomType->id}}">{{$roomType->roomType}}</option>
		@endforeach> -->
	    </select>
	  </div>
	</div>
	@endif

	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label">Total Rooms</label>
	  <div class="controls">
	    <input id="totalRooms" name="totalRooms" type="text" placeholder="Total Rooms" class="input-xlarge" required="" value="@if(empty($rooms)) {{$editRoomManager->totalRooms}} @endif">
	    
	  </div>
	</div>

	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">No. of Adult</label>
	  <div class="controls">
	    <select id="adult" name="adult" class="input-xlarge">
	      <option value="0">Zero</option>
	      <option value="1">One</option>
	      <option value="2">Two</option>
	      <option value="3">Three</option>
	    </select>
	  </div>
	</div>

	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label">No of Child</label>
	  <div class="controls">
	    <input id="child" name="child" type="text" placeholder="No of Child" class="input-xlarge" value="@if(empty($rooms)) {{$editRoomManager->child}} @endif">
	    
	  </div>
	</div>

	<!-- Button -->
	<div class="control-group">
	  <label class="control-label">Save</label>
	  <div class="controls">
	    <button id="" class="btn btn-primary">Save</button>
	  </div>
	</div>

	</fieldset>

	



	{{Form::close()}}

	</div>
@stop

@section('js')
	@parent
	
	<script type="text/javascript">
	$('#hotelID').on('change', function(){
		var hotelSelect = $(this);
		var hotelID = $(this).val();
		$.ajax({
			url: "{{URL::to('/ajax/get-room-types')}}",
			type: "POST",
			data: { hotelID: hotelID}
		}).done(function(html){
			hotelSelect.closest('.control-group').next('.control-group').find('#roomTypeID').html(html);
		});

	});

	</script>

@stop