@extends('layout.one')

@section('mainview')
<?php //dd($hotels) ?>
	<h1>Add/Edit Hotel</h1>
@if(!empty($hotels))
<div class="btn-toolbar">
    <a class="btn btn-primary" href="#addHotel">New Hotel</a>
<!--     <button class="btn">Import</button>
    <button class="btn">Export</button> -->
</div>

<div class="well">
    <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Hotel Name</th>
          <th>Address</th>
          <th>City</th>
          <th>State</th>
          <th>Country</th>
          <th>ZIP</th>
          <th>Phone</th>
          <th>FAX</th>
          <th>Email</th>
          <th>TAX %</th>
          <th style="width: 36px;"></th>
        </tr>
      </thead>
      <tbody>
      	@foreach($hotels as $hotel)
        <tr>
          <td>{{$hotel->id}}</td>
          <td>{{$hotel->name}}</td>
          <td>{{$hotel->address}}</td>
          <td>{{$hotel->city}}</td>
          <td>{{$hotel->state}}</td>
          <td>{{$hotel->country}}</td>
          <td>{{$hotel->zip}}</td>
          <td>{{$hotel->phone}}</td>
          <td>{{$hotel->fax}}</td>
          <td>{{$hotel->email}}</td>
          <td>{{$hotel->tax}}</td>
          <td>
              <a href="{{URL::to('admin/edit-hotel/'.$hotel->id)}}"><i class="icon-pencil"></i></a>
              <a href="#myModal" role="button" data-toggle="modal" data-hotel="{{$hotel->id}}"><i class="icon-remove"></i></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
</div>
<!-- <div class="pagination">
    <ul>
        <li><a href="#">Prev</a></li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">Next</a></li>
    </ul>
</div> -->
@endif
	@if(!empty($hotels)) 
	{{Form::open(array('url' => 'admin/add-hotel'))}}
	@else
	{{Form::open(array('url' => 'admin/edit-hotel'))}}
	@endif
<fieldset>

<!-- Form Name -->
<legend>Add Hotel</legend>
<div class="row" id="addHotel">
	<div class="span6">
		@if(!empty($editHotel))  
			<input type="hidden" name="hotelID" value="{{ $editHotel->id }}">
		@endif
		<!-- Text input-->
		<div class="control-group">
		  <label class="control-label">Name</label>
		  <div class="controls">
		    <input id="name" name="name" type="text" placeholder="Name" class="input-xlarge" required="" value="@if(!empty($editHotel)) {{ $editHotel->name }} @endif">
		    
		  </div>
		</div>

		<!-- Appended Input-->
		<div class="control-group">
		  <label class="control-label">TAX Rate</label>
		  <div class="controls">
		    <div class="input-append">
		      <input id="tax" name="tax" class="span2" placeholder="TAX" type="text" required="" value="@if(!empty($editHotel)) {{ $editHotel->tax }} @endif">
		      <span class="add-on">%</span>
		    </div>
		    
		  </div>
		</div>

		<!-- Textarea -->
		<div class="control-group"> 
		  <label class="control-label">Address</label>
		  <div class="controls">                     
		    <textarea id="address" name="address" required="">@if(!empty($editHotel)) {{ $editHotel->address }} @endif</textarea>
		  </div>
		</div>

		<!-- Text input-->
		<div class="control-group">
		  <label class="control-label">City</label>
		  <div class="controls">
		    <input id="city" name="city" type="text" placeholder="City" class="input-xlarge" required="" value="@if(!empty($editHotel)) {{ $editHotel->city }} @endif">
		    
		  </div>
		</div>

		<!-- Text input-->
		<div class="control-group">
		  <label class="control-label">State</label>
		  <div class="controls">
		    <input id="state" name="state" type="text" placeholder="State" class="input-xlarge" required="" value="@if(!empty($editHotel)) {{ $editHotel->state }} @endif">
		    
		  </div>
		</div>

		<!-- Text input-->
		<div class="control-group">
		  <label class="control-label">Country</label>
		  <div class="controls">
		    <input id="country" name="country" type="text" placeholder="Country" class="input-xlarge" required="" value="@if(!empty($editHotel)) {{ $editHotel->country }} @endif">
		    
		  </div>
		</div>

	</div> <!-- /span6 -->
	
	<div class="span6">

		<!-- Text input-->
		<div class="control-group">
		  <label class="control-label">ZIP</label>
		  <div class="controls">
		    <input id="zip" name="zip" type="text" placeholder="ZIP" class="input-xlarge" required="" value="@if(!empty($editHotel)) {{ $editHotel->zip }} @endif">
		    
		  </div>
		</div>

		<!-- Text input-->
		<div class="control-group">
		  <label class="control-label">Phone Number</label>
		  <div class="controls">
		    <input id="phone" name="phone" type="text" placeholder="Phone Number" class="input-xlarge" required="" value="@if(!empty($editHotel)) {{ $editHotel->phone }} @endif">
		    
		  </div>
		</div>

		<!-- Text input-->
		<div class="control-group">
		  <label class="control-label">FAX</label>
		  <div class="controls">
		    <input id="fax" name="fax" type="text" placeholder="FAX" class="input-xlarge" required=""value="@if(!empty($editHotel)) {{ $editHotel->fax }} @endif">
		    
		  </div>
		</div>

		<!-- Text input-->
		<div class="control-group">
		  <label class="control-label">Email Address</label>
		  <div class="controls">
		    <input id="email" name="email" type="email" placeholder="Email Address" class="input-xlarge" required="" value="@if(!empty($editHotel)) {{ $editHotel->email }} @endif">
		    
		  </div>
		</div>


	</div> <!-- /span6 -->
</div>

<!-- Button -->
<div class="control-group">
  <label class="control-label">Save</label>
  <div class="controls">
    <button id="singlebutton" class="btn btn-primary btn-large">Save</button>
  </div>
</div>

</fieldset>

	{{Form::close()}}
	
@stop

@section('modal')
<div class="modal small hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Delete Confirmation</h3>
    </div>
    <div class="modal-body">
        <p class="error-text">Are you sure you want to delete the Hotel?</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-danger" data-dismiss="modal">Delete</button>
    </div>
</div>
@stop