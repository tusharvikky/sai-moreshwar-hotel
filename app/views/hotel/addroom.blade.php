@extends('layout.one')

@section('mainview')
	<h1>Add/Edit Room</h1>

	@if(!empty($roomTypes))
	<div class="btn-toolbar">
	    <a class="btn btn-primary" href="#addRoom">New Room</a>
<!-- 	    <button class="btn">Import</button>
	    <button class="btn">Export</button> -->
	</div>
		
	<div class="well">
	    <table class="table">
	      <thead>
	        <tr>
	          <th>#</th>
	          <!-- <th>Hotel ID</th> -->
	          <th>Hotel Name</th>
	          <th>Room Type</th>
	          <th>Room Description</th>
	          <th style="width: 36px;"></th>
	        </tr>
	      </thead>
	      <tbody>
	      	@foreach($roomTypes as $roomType)
	        <tr>
	          <td>{{$roomType->id}}</td>
	          <!-- <td>{{ $roomType->hotelID }}</td> -->
	          <td><?php $name = Hotel::getHotel($roomType->hotelID) ?>{{$name["name"]}}</td>
	          <td>{{$roomType->roomType}}</td>
	          <td>{{$roomType->descriptionRoom}}</td>
	          <td>
	              <a href="{{URL::to('admin/edit-room-type/'.$roomType->id)}}"><i class="icon-pencil"></i></a>
	              <a href="#myModal" role="button" data-toggle="modal" data-hotel="{{$roomType->id}}"><i class="icon-remove"></i></a>
	          </td>
	        </tr>
	        @endforeach
	      </tbody>
	    </table>
	</div>
	@endif

	@if(!empty($roomTypes))
	<div id="addRoom">
		{{Form::open(array('url' => 'admin/add-room-type'))}}
	@else
		{{Form::open(array('url' => 'admin/edit-room-type'))}}
	@endif
	<!-- Form Name -->
	<legend>Add Room</legend>

	@if(!empty($editroomType))
		<input type="hidden" name="RoomTypeID" value="{{ $editroomType->id }}">
		<select name="hotelID">
				<option value="{{$editroomType->hotelID}}"><?php $name = Hotel::getHotel($editroomType->hotelID) ?>{{$name["name"]}}</option>
		</select>		
	@else
	<select name="hotelID">
		@foreach($hotels as $hotel)
			<option value="{{$hotel->id}}">{{$hotel->name}}</option>
		@endforeach
	</select>
	@endif
	<fieldset>

	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label">Room Type</label>
	  <div class="controls">
	    <input id="roomType" name="roomType" type="text" placeholder="Room Type" class="input-xlarge" required="" value="@if(!empty($editroomType)) {{$editroomType->roomType}} @endif">
	    
	  </div>
	</div>

	<!-- Textarea -->
	<div class="control-group">
	  <label class="control-label">Description/Facilities</label>
	  <div class="controls">                     
	    <textarea id="descriptionRoom" name="descriptionRoom">@if(!empty($editroomType)) {{$editroomType->descriptionRoom}} @endif</textarea>
	  </div>
	</div>

	<!-- Button -->
	<div class="control-group">
	  <label class="control-label">Save</label>
	  <div class="controls">
	    <button id="" name="" class="btn btn-success">Save</button>
	  </div>
	</div>

	</fieldset>

	

	{{Form::close()}}
	</div>

@stop