@extends('layout.one')

@section('mainview')
<?php //dd($prices) ?>
	<h1>Price Plan Manager</h1>

	@if(empty($editPriceManager))
	<div class="btn-toolbar">
	    <a class="btn btn-primary" href="#addPrice">Add Price</a>
	<!--     <button class="btn">Import</button>
	    <button class="btn">Export</button> -->
	</div>

	{{Form::open(array('url' => 'ajax/price-plan-manager', 'id' => 'getPrice'))}}
<!-- 	<div class="row">
			<select name="hotelID" id="hotelID">
				<option selected="selected">Select Hotel</option>
				@foreach($hotels as $hotel)
					<option value="{{$hotel->id}}">{{$hotel->name}}</option>
				@endforeach
			</select>

			<select name="roomType" id="roomType">

			</select>

			<button class="btn btn-success" style="margin-top:-10px;">Get Price</button>
	</div> -->
	{{Form::close()}}

<table class="table well">
				<thead>
					<tr>
						<th>Hotel</th>
						<th>Room Type</th>
						<th>Extra</th>
						<th>Saturday</th>
						<th>Sunday</th>
						<th>Monday</th>
						<th>Tuesday</th>
						<th>Wednesday</th>
						<th>Thursday</th>
						<th>Friday</th>
						<th style="width: 36px;"></th>
					</tr>
				</thead>
				<tbody>
					@foreach($prices as $price)
					<tr id="priceValue">
						<?php $name = Hotel::getHotel($price->hotelID); ?>
						<th>{{$name["name"]}}</th>
						<?php $room = Room::getRoomType($price->roomType); ?>
						<th>{{$room["roomType"]}}</th>
						<th>{{$price->extra}}</th>
						<th>{{$price->sat}}</th>
						<th>{{$price->sun}}</th>
						<th>{{$price->mon}}</th>
						<th>{{$price->tue}}</th>
						<th>{{$price->wed}}</th>
						<th>{{$price->thu}}</th>
						<th>{{$price->fri}}</th>
						<th><a href="{{URL::to('admin/edit-price-manager/'.$price->hotelID.'/'.$price->roomType)}}"><i class='icon-pencil'></i></a></th>
					</tr>
					@endforeach
				</tbody>
			</table>
	@endif

	<legend>Add/Edit Room Price</legend>
	@if(empty($editPriceManager))
	{{ Form::open(array('url' => 'admin/price-manager', 'id' => 'addPrice'))}}
			<div class="row">
			<select name="hotelID" id="hotelIDAdd" required="required">
					<option selected="selected">Select Hotel</option>
				@foreach($hotels as $hotel)
					<option value="{{$hotel->id}}">{{$hotel->name}}</option>
				@endforeach
			</select>

			<select name="roomType" id="roomTypeAdd" required="required">

			</select>
	@else
	{{ Form::open(array('url' => 'admin/edit-price-manager'))}}

			<div class="row">
			<select name="hotelID" id="hotelIDAdd" required="required">
					<option value="{{$editPriceManager->hotelID}}"><?php $name = Hotel::getHotel($editPriceManager->hotelID); ?>{{$name["name"]}}</option>
			</select>

			<select name="roomType" id="roomTypeAdd" required="required">
					<option value="{{$editPriceManager->roomType}}"><?php $room = Room::getRoomType($editPriceManager->roomType); ?>{{$room["roomType"]}}</option>
			</select>
	@endif 


			<table class="table well">
				<thead>
					<tr>
						<th>For</th>
						<th>Saturday</th>
						<th>Sunday</th>
						<th>Monday</th>
						<th>Tuesday</th>
						<th>Wednesday</th>
						<th>Thursday</th>
						<th>Friday</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>Basic Adult</th>
						<th>Rs. <input class="span1" type="text" name="adult[sat]" required="required" value="@if(!empty($editPriceManager)) {{$editPriceManager->sat}} @endif"></th>
						<th>Rs. <input class="span1" type="text" name="adult[sun]" required="required" value="@if(!empty($editPriceManager)) {{$editPriceManager->sun}} @endif"></th>
						<th>Rs. <input class="span1" type="text" name="adult[mon]" required="required" value="@if(!empty($editPriceManager)) {{$editPriceManager->mon}} @endif"></th>
						<th>Rs. <input class="span1" type="text" name="adult[tue]" required="required" value="@if(!empty($editPriceManager)) {{$editPriceManager->tue}} @endif"></th>
						<th>Rs. <input class="span1" type="text" name="adult[wed]" required="required" value="@if(!empty($editPriceManager)) {{$editPriceManager->wed}} @endif"></th>
						<th>Rs. <input class="span1" type="text" name="adult[thu]" required="required" value="@if(!empty($editPriceManager)) {{$editPriceManager->thu}} @endif"></th>
						<th>Rs. <input class="span1" type="text" name="adult[fri]" required="required" value="@if(!empty($editPriceManager)) {{$editPriceManager->fri}} @endif"></th>
					</tr>
					<tr>
						<th>Extra</th>
						<th colspan=8>Rs. <input class="span11" type="text" name="extra" required="required" value="@if(!empty($editPriceManager)) {{$editPriceManager->extra}} @endif"> </th>
<!-- 						<th>Rs. <input class="span1" type="text" name="child" required="required"></th>
						<th>Rs. <input class="span1" type="text" name="childsun" required="required"></th>
						<th>Rs. <input class="span1" type="text" name="childmon" required="required"></th>
						<th>Rs. <input class="span1" type="text" name="childtue" required="required"></th>
						<th>Rs. <input class="span1" type="text" name="childwed" required="required"></th>
						<th>Rs. <input class="span1" type="text" name="childthu" required="required"></th>
						<th>Rs. <input class="span1" type="text" name="childfri" required="required"></th> -->
					</tr>
				</tbody>
			</table>
		</div>

		<button class="btn btn-primary">Save</button>
	{{Form::close()}}

@stop


@section('js')
	@parent
	
	<script type="text/javascript">
	$('#hotelID').on('change', function(){
		var hotelSelect = $(this);
		var hotelID = $(this).val();
		$.ajax({
			url: "{{URL::to('/ajax/get-room-types')}}",
			type: "POST",
			data: { hotelID: hotelID}
		}).done(function(html){
			hotelSelect.next('#roomType').html(html);
		});

	});

	$('#getPrice').on('submit', function(e){
		e.preventDefault();
		var form = $(this);
		var data = form.serialize();
		var url = form.attr('action');
		// console.log();
		$.ajax({
			url: url,
			type: "POST",
			data: data
		}).done(function(html){
			form.siblings('.table').children().children('tr#priceValue').html(html);
		});
	});


	$('#hotelIDAdd').on('change', function(){
		var hotelSelect = $(this);
		var hotelID = $(this).val();
		$.ajax({
			url: "{{URL::to('/ajax/get-room-types')}}",
			type: "POST",
			data: { hotelID: hotelID}
		}).done(function(html){
			hotelSelect.next('#roomTypeAdd').html(html);
		});

	});

	</script>

@stop