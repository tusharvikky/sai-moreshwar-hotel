@extends('layout.one')

@section('mainview')
	<h1>Special Offer</h1>
	@if(empty($editSpecialOffer))
	<div class="btn-toolbar">
	    <a class="btn btn-primary" href="#addOffer">New Offer</a>
<!-- 	    <button class="btn">Import</button>
	    <button class="btn">Export</button -->>
	</div>
	<div class="well">
	    <table class="table">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>Hotel Name</th>
	          <th>Room Type</th>
	          <th>Offer Name</th>
	          <th>Start Date (YYYY/MM/DD)</th>
	          <th>End Date (YYYY/MM/DD)</th>
	          <th>Rate</th>
	          <th>Extra</th>

	          <th style="width: 36px;"></th>
	        </tr>
	      </thead>
	      <tbody>
	      	@foreach($offers as $offer)
	        <tr>
	          <td>{{$offer->id}}</td>
	          <td><?php $name = Hotel::getHotel($offer->hotelID); ?>{{$name["name"]}}</td>
	          <td><?php $room = Room::getRoomType($offer->roomType) ?>{{$room["roomType"]}}</td>
	          <td>{{$offer->offer}}</td>
	          <td>{{$offer->startDate}}</td>
	          <td>{{$offer->endDate}}</td>
	          <td>{{$offer->rate}}</td>
	          <td>{{$offer->extra}}</td>
	          <td>
	              <a href="{{URL::to('admin/edit-special-offer/'.$offer->id)}}"><i class="icon-pencil"></i></a>
	          </td>
	        </tr>
	        @endforeach
	      </tbody>
	    </table>
	</div>
	@endif



	@if(empty($editSpecialOffer))
	{{Form::open(array('url' => 'admin/special-offer'))}}
	<!-- Form Name -->
	<legend id="addOffer">Add/Edit Special Offer</legend>

	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">Hotel</label>
	  <div class="controls">
	    <select id="hotelID" name="hotelID" class="input-xlarge">
	    	<option selected="selected">Select Hotel</option>
		@foreach($hotels as $hotel)
			<option value="{{$hotel->id}}">{{$hotel->name}}</option>
		@endforeach
	    </select>
	  </div>
	</div>

	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">Room Type</label>
	  <div class="controls">
	    <select id="roomType" name="roomType" class="input-xlarge">

	    </select>
	  </div>
	</div>	
	@else
	{{Form::open(array('url' => 'admin/edit-special-offer'))}}
	<!-- Form Name -->
	<legend id="addOffer">Add/Edit Special Offer</legend>

	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">Hotel</label>
	  <div class="controls">
	    <select id="hotelID" name="hotelID" class="input-xlarge">
			<option value="{{$editSpecialOffer->hotelID}}"><?php $name = Hotel::getHotel($editSpecialOffer->hotelID);?>{{$name["name"]}}</option>
	    </select>
	  </div>
	</div>

	<!-- Select Basic -->
	<div class="control-group">
	  <label class="control-label">Room Type</label>
	  <div class="controls">
	    <select id="roomType" name="roomType" class="input-xlarge">
	    	<option value="{{$editSpecialOffer->roomType}}"><?php $room = Room::getRoomType($editSpecialOffer->roomType); ?>{{$room["roomType"]}}</option>
	    </select>
	  </div>
	</div>

	@endif
	<fieldset>

	

	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label">Offer Name</label>
	  <div class="controls">
	    <input id="offer" name="offer" type="text" placeholder="Offer Name" class="input-xlarge" required="" value="@if(!empty($editSpecialOffer)) {{$editSpecialOffer->offer}} @endif">
	    
	  </div>
	</div>

	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label">Date</label>
	  <div class="controls">
	    <input id="date" name="date" type="text" placeholder="Date" class="input-xlarge" required="" value="@if(!empty($editSpecialOffer)) {{$editSpecialOffer->startDate}} - {{$editSpecialOffer->endDate}} @endif">
	    
	  </div>
	</div>

	<!-- Prepended text-->
	<div class="control-group">
	  <label class="control-label">Rate</label>
	  <div class="controls">
	    <div class="input-prepend">
	      <span class="add-on">Rs.</span>
	      <input id="rate" name="rate" class="span2" placeholder="Rate" type="text" required="" value="@if(!empty($editSpecialOffer)) {{$editSpecialOffer->rate}} @endif">
	    </div>
	    
	  </div>
	</div>

	<!-- Prepended text-->
	<div class="control-group">
	  <label class="control-label">Extra</label>
	  <div class="controls">
	    <div class="input-prepend">
	      <span class="add-on">Rs.</span>
	      <input id="extra" name="extra" class="span2" placeholder="Extra" type="text" required="" value="@if(!empty($editSpecialOffer)) {{$editSpecialOffer->extra}} @endif">
	    </div>
	    
	  </div>
	</div>

	<!-- Button -->
	<div class="control-group">
	  <label class="control-label">Save</label>
	  <div class="controls">
	    <button class="btn btn-primary">Save</button>
	  </div>
	</div>

	</fieldset>
	{{Form::close()}}

@stop


@section('js')
	@parent
	{{HTML::script('assets/moment.min.js')}}
	{{HTML::script('assets/daterangepicker.js')}}
	{{HTML::style('assets/daterangepicker.css')}}

	<script type="text/javascript">
	$('#hotelID').on('change', function(){
		var hotelSelect = $(this);
		var hotelID = $(this).val();
		$.ajax({
			url: "{{URL::to('/ajax/get-room-types')}}",
			type: "POST",
			data: { hotelID: hotelID}
		}).done(function(html){
			hotelSelect.closest('.control-group').next('.control-group').find('#roomType').html(html);
		});

	});

	$(document).ready(function() {
	  $('input[name="date"]').daterangepicker();
	});
	</script>

@stop